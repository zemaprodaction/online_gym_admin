FROM node:14.9.0-alpine3.10 as builder
WORKDIR /app
COPY package*.json ./
RUN npm install --no-package-lock
COPY . .
RUN npm run build
EXPOSE 80
CMD ["npm", "start"]

