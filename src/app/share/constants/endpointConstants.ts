import {environment} from '../../../environments/environment';

class RootConstants {
  public static GATEWAY = `${environment.api.baseUrl}`;
}

export class TrainingParamConstants {
  public static WORKOUT_PROGRAM_ID_TEMPLATE = '{workoutProgramId}';
  static WORKOUT_INSTANCE_ID = 'workoutInstanceId';
  static WORKOUT_PROGRAM_ID = 'workoutProgramId';
  static EXERCISE_ID = 'exerciseId';
}

export class TrainingEndpointConstants {
  public static ROOT_WORKOUT_PROGRAM_SERVICE = `${RootConstants.GATEWAY}/workout-program`;
  public static WORKOUT_PROGRAM = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/workoutProgram`;
  public static WORKOUT = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/workout`;
  public static EXERCISES = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/exercise`;
  public static GOALS = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/metadata/goal`;
  public static MEASURE_TYPES = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/metadata/measureTypes`;
  public static MUSCLES = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/metadata/muscles`;
  public static METADATA = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/metadata`;
  public static FILTER = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/filter`;
}

export class StatisticConstants {
  public static REMOVE_USER_STATISTIC = `${RootConstants.GATEWAY}/statistic/user`;
}

export class AuthorizationConstants {
  public static AUTHORIZATION = `${RootConstants.GATEWAY}/workout-program/user_profile/me`;
}

export class UserEndpointConstants {
  public static USER = '/user';
  public static USER_WORKOUT_PROGRAMS =
    `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}${UserEndpointConstants.USER}/workout-programs`;
  public static ENROLL = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}${UserEndpointConstants.USER}/enroll`;
  public static KEYKLOAk_API_USERS = `${RootConstants.GATEWAY}/users`;
}

export class MetaDataUserEndpointConstants {
  public static METADATA = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/metadata`;
  public static EQUIPMENT = `${MetaDataUserEndpointConstants.METADATA}/equipment`;
}
