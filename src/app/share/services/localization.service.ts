import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {TrainingEndpointConstants} from '../constants/endpointConstants';


@Injectable({providedIn: 'root'})
export class LocalizationService {

  constructor(private http: HttpClient) {
  }

  public updateLocalization(local) {
    return this.http.put(`${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/localization`, local, {
      headers: new HttpHeaders().set('accept-language', 'en'),
    }).pipe(catchError((error: any) => throwError(error.json())));
  }

  getRuString(key: string) {
    return this.http.get(`${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/localization/${key}/ru`,
      {responseType: 'text'});
  }
}
