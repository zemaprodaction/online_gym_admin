import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import * as fromConstants from '../constants/endpointConstants';
import {MeasureType} from '../../trainings/models/measureType';
import {Goal} from '../type/goal';
import {CharacteristicPropertyFilter} from '../../trainings/models/characteristic-property-filter';
import {Muscle} from '../type/muscle';


@Injectable({providedIn: 'root'})
export class MetadataService {

  constructor(private http: HttpClient) {
  }

  public getMeasureTypes(): Observable<MeasureType[]> {
    return this.http.get<MeasureType[]>(`${fromConstants.TrainingEndpointConstants.MEASURE_TYPES}`)
      .pipe(
        catchError((error: any) => throwError(error))
      );
  }

  public getMuscles(): Observable<Muscle[]> {
    return this.http.get<Muscle[]>(`${fromConstants.TrainingEndpointConstants.MUSCLES}`)
      .pipe(
        catchError((error: any) => throwError(error))
      );
  }


  public saveMuscle(muscle: Muscle): Observable<Muscle> {
    return this.http.post<Muscle>(`${fromConstants.TrainingEndpointConstants.MUSCLES}`, muscle)
      .pipe(
        catchError((error: any) => throwError(error))
      );
  }

  public getValueOfCharacteristicByEntityName(entityName: string): Observable<CharacteristicPropertyFilter[]> {
    return this.http.get<CharacteristicPropertyFilter[]>(`${fromConstants.TrainingEndpointConstants.FILTER}/${entityName}`)
      .pipe(
        catchError((error: any) => throwError(error))
      );
  }

  public getGoals(): Observable<Goal[]> {
    return this.http.get<Goal[]>(`${fromConstants.TrainingEndpointConstants.GOALS}`)
      .pipe(
        catchError((error: any) => throwError(error))
      );
  }

  saveEntity(entityName: string, val: any) {
    return this.http.post<any>(`${fromConstants.TrainingEndpointConstants.METADATA}/${entityName}`, val)
      .pipe(
        catchError((error: any) => throwError(error))
      );
  }

  getEntities(entityName: string) {
    return this.http.get<any[]>(`${fromConstants.TrainingEndpointConstants.METADATA}/${entityName}`)
      .pipe(
        catchError((error: any) => throwError(error))
      );
  }

  removeEntity(entityName: string, $event: string) {
    return this.http.delete<string>(`${fromConstants.TrainingEndpointConstants.METADATA}/${entityName}/${$event}`)
      .pipe(catchError((error: any) => {
        return throwError(error.json());
      }));
  }
}
