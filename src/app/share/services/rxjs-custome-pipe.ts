import {pipe} from 'rxjs';
import {filter, flatMap} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {DialogService} from './dialog.service';

@Injectable({
  providedIn: 'root'
})
export class RxjsCustomPipeService {

  constructor(private dialogService: DialogService) {
  }
  askIfYouWantRemovePipe(): any {
  return () => pipe(
    flatMap(value => {
      const dialogRef = this.dialogService.openDialog(value);
      return dialogRef.afterClosed();
    }),
    filter(x => x != null),
  );
}
}
