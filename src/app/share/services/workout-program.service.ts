import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';

import {WorkoutProgram} from '../../trainings/models/workout-programs';
import {Observable, throwError} from 'rxjs';
import * as fromConstants from '../constants/endpointConstants';


@Injectable({providedIn: 'root'})
export class WorkoutProgramService {

  constructor(private http: HttpClient) {
  }

  public getWorkoutPrograms(): Observable<WorkoutProgram[]> {
    return this.http.get<WorkoutProgram[]>(`${fromConstants.TrainingEndpointConstants.WORKOUT_PROGRAM}`)
      .pipe(
        catchError((error: any) => throwError(error))
      );
  }

  public getWorkoutProgram(id: String): Observable<WorkoutProgram> {
    return this.http.get<WorkoutProgram>(`${fromConstants.TrainingEndpointConstants.WORKOUT_PROGRAM}/${id}`)
      .pipe(catchError((error: any) => throwError(error)));
  }

  public removeWorkoutProgram(workoutProgram: WorkoutProgram): Observable<string> {
    return this.http.delete<string>(`${fromConstants.TrainingEndpointConstants.WORKOUT_PROGRAM}/${workoutProgram.id}`)
      .pipe(catchError((error: any) => {
        return throwError(error.json());
      }));
  }

  public createWorkoutProgram(workoutProgram: WorkoutProgram): Observable<WorkoutProgram> {
    return this.http.post<WorkoutProgram>(`${fromConstants.TrainingEndpointConstants.WORKOUT_PROGRAM}`, workoutProgram)
      .pipe(catchError((error: any) => throwError(error.json())));
  }

  public updateWorkoutProgram(workoutProgram: WorkoutProgram): Observable<WorkoutProgram> {
    return this.http.put<WorkoutProgram>(`${fromConstants.TrainingEndpointConstants.WORKOUT_PROGRAM}`, workoutProgram)
      .pipe(catchError((error: any) => throwError(error.json())));
  }

  public getUserWorkoutPrograms(): Observable<WorkoutProgram[]> {
    return this.http.get<WorkoutProgram[]>(`${fromConstants.UserEndpointConstants.USER_WORKOUT_PROGRAMS}`)
      .pipe(
        catchError((error: any) => throwError(error))
      );
  }

}
