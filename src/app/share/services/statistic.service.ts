import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import * as fromConstants from '../constants/endpointConstants';


@Injectable({providedIn: 'root'})
export class StatisticService {

  constructor(private http: HttpClient) {
  }


  public removeAllUserStatistic(userId: string) {
    return this.http.delete<string>(`${fromConstants.StatisticConstants.REMOVE_USER_STATISTIC}/${userId}`)
      .pipe(catchError((error: any) => {
        return throwError(error.json());
      }));
  }


}
