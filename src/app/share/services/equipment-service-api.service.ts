import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import * as fromConstants from '../constants/endpointConstants';
import {Equipment} from '../type/equipment';


@Injectable({providedIn: 'root'})
export class EquipmentServiceApi {

  constructor(private http: HttpClient) {
  }


  public getAllEquipment() {
    return this.http.get<Equipment[]>(`${fromConstants.MetaDataUserEndpointConstants.EQUIPMENT}`)
      .pipe(catchError((error: any) => {
        return throwError(error.json());
      }));
  }

  public addEquipment(equipment: Equipment) {
    return this.http.post<Equipment>(`${fromConstants.MetaDataUserEndpointConstants.EQUIPMENT}`, equipment)
      .pipe(catchError((error: any) => {
        return throwError(error.json());
      }));
  }


}
