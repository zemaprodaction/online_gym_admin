import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { EnumToArrayPipe } from './pipe/enum-to-array.pipe';

@NgModule({
  declarations: [
  EnumToArrayPipe],
  imports: [
    CommonModule,
    MatSnackBarModule
  ],
    exports: [
        EnumToArrayPipe
    ]
})
export class ShareModule { }
