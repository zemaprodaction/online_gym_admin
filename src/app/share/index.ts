import {WorkoutProgramService} from './services/workout-program.service';

export const services: any[] = [ WorkoutProgramService];

export * from './services/workout-program.service';
