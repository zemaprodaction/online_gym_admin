export interface LocalizableField {
  language: string;
  string: string;
}
