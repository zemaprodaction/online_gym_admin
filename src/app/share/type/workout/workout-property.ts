export interface WorkoutProperty {
  id: string;
  workoutPropertyType: string;
  name: string;
  type: string;
}

export enum WorkoutPropertyTypeKey {
  WORKOUT_TYPE= 'workoutType',
  WORKOUT_PLACE= 'workoutPlace',
  WORKOUT_DIFFICULTY = 'workoutDifficulty',
  EQUIPMENT_NECESSITY = 'equipmentNecessity',
}
