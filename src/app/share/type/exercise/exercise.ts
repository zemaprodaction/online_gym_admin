import {AdditionalInfo} from './additional-info';
import {Video} from './video';

export class Exercise {
  constructor(public imageBundle: ImageBundle = {mobileImageLink: '', webImageLink: ''}) {
  }

  id: string;
  name: string;
  exerciseType: string;
  additionalInfo: AdditionalInfo;
  detailedDescription: string;
  video: Video;
}

export class ImageBundle {
  mobileImageLink: string;
  webImageLink: string;
}
