import {Equipment} from '../equipment';

export interface AdditionalInfo {
  equipment: Equipment[];
  musclesInvolved: string[];
  targetMuscle: string[];
}
