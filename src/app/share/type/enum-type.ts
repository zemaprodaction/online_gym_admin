export interface EnumType {
  code: string;
  value: string;
}
