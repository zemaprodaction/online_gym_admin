import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {appRoutesNames} from './routes.names';
import {CustomPreloadingStrategy} from './custom-preloading-strategy';

export const APP_ROUTES: Routes = [
  { path: '', pathMatch: 'full', redirectTo: `${appRoutesNames.ADMIN}` },
  {
    path: `${appRoutesNames.ADMIN}`,
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
  },
];

export const routing: ModuleWithProviders<RouterModule> = RouterModule.forRoot(
  APP_ROUTES,
  {
    preloadingStrategy: CustomPreloadingStrategy
  }
);
