import {TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import * as fromAction from '../actions/router.action';
import * as fromEffect from './router.effects';
import {provideMockActions} from '@ngrx/effects/testing';
import {ReplaySubject} from 'rxjs';

import {Actions} from '@ngrx/effects';
import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import Spy = jasmine.Spy;

@Component({
  template: `<p>test-teplate</p>`
})
class TestComponent {
}

describe('RouterEffects', () => {

  let effects: fromEffect.RouterEffects;
  let actions$: ReplaySubject<any>;
  let router: Router;
  let navigateSpy: Spy;
  let location: Location;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {path: 'test', component: TestComponent},
        ]),
      ],
      providers: [
        fromEffect.RouterEffects,
        provideMockActions(() => actions$),
      ],
      declarations: [
        TestComponent
      ]
    });

    location = TestBed.get(Location);
    router = TestBed.get(Router);
    actions$ = TestBed.get(Actions);
    effects = TestBed.get(fromEffect.RouterEffects);
    navigateSpy = spyOn(TestBed.get(Router), 'navigate');
  });

  it('should navigate to url', async () => {

    actions$ = new ReplaySubject(1);
    actions$.next(new fromAction.Go({path: ['test']}));

    effects.navigate$.subscribe((result) => {
      expect(result).toEqual({path: ['test']});
      expect(navigateSpy).toHaveBeenCalledWith(['test'], {queryParams: undefined});
    });
  });

  it('should call back method of Location', async () => {

    actions$ = new ReplaySubject(1);
    actions$.next(new fromAction.Back());
    spyOn(location, 'back');

    effects.navigateBack$.subscribe((result) => {
      expect(location.back).toHaveBeenCalledTimes(1);
    });
  });

  it('should call forward method of Location', async () => {

    actions$ = new ReplaySubject(1);
    actions$.next(new fromAction.Forward());
    spyOn(location, 'forward');

    effects.navigateForward$.subscribe((result) => {
      expect(location.forward).toHaveBeenCalledTimes(1);
    });
  });
});
