import {CustomSerializer, RouterStateUrl} from './index';
import {Params} from '@angular/router';

class MockActiveStateSnapshot {
  queryParams?: Params;
  params?: Params;
  firstChild: MockActiveStateSnapshot | null;
}

class MockRouterStateSnapshot {
  url: string;
  root: MockActiveStateSnapshot;
}

describe('CustomSerialization: unit', () => {

  let serializer: CustomSerializer;

  const expected: RouterStateUrl = {
    url: 'this-is-url',
    queryParams: {},
    params: {}
  };
  const input: MockRouterStateSnapshot = {
    url: 'this-is-url',
    root: {
      queryParams: {},
      firstChild: {
        firstChild: null,
        params: {}
      }
    }
  };

  beforeEach(() => {
    serializer = new CustomSerializer();
  });

  describe('serialize', () => {
    it('should return only URI', () => {
      expect(serializer.serialize(input as any)).toEqual(expected);
    });

    it('should return route and query params', () => {
      const paramsObject = {
        param1: 'val1',
        param2: 'val2'
      };
      expected.queryParams = {...paramsObject};
      expected.params = {...paramsObject};

      input.root.queryParams = {...paramsObject};
      input.root.firstChild = {
        firstChild: {
          firstChild: {
            firstChild: {
              firstChild: null,
              params: {...paramsObject}
            }
          }
        }
      };
    expect(serializer.serialize(input as any)).toEqual(expected);
    });
  });
});

