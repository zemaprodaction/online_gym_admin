import * as RouterActions from '../actions/router.action';

describe('Router Actions', () => {
  it('should create a GO action', () => {
    const payload = {
      path: ['test-patch']
    };
    const action = new RouterActions.Go(payload);

    expect({...action}).toEqual({
      type: RouterActions.GO,
      payload
    });
  });

  it('should create a BACK action', () => {
    const action = new RouterActions.Back();

    expect({...action}).toEqual({
      type: RouterActions.BACK,
    });
  });

  it('should create a FORWARD action', () => {
    const action = new RouterActions.Forward();

    expect({...action}).toEqual({
      type: RouterActions.FORWARD,
    });
  });

});
