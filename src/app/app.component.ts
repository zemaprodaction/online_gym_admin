import {Component, OnInit} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {Store} from '@ngrx/store';
import {AuthorizationState} from './authorization/store/reducers';
import {selectAuthorizationUser} from './authorization/store/selectors/authorization.selector';
import {getAuthorization} from './authorization/store/actions/authorization.actions';
import {Observable} from 'rxjs';
import {User} from './authorization/model/user';
import * as fromStore from './trainings/store';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  user: Observable<User>;

  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer, private _store: Store<AuthorizationState>) {
    this.matIconRegistry.addSvgIcon(
      'cardiogram',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/cardiogram.svg')
    );
  }

  ngOnInit(): void {
    this._store.dispatch(getAuthorization());

    this.user = this._store.select(selectAuthorizationUser);
  }
}
