import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import * as fromConstants from '../../share/constants/endpointConstants';
import {catchError} from 'rxjs/operators';
import {UserKeycloak} from '../models/user-keycloak';

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {
  }

  public getUsers(): Observable<UserKeycloak[]> {
    return this.http.get<UserKeycloak[]>(`${fromConstants.UserEndpointConstants.KEYKLOAk_API_USERS}`)
      .pipe(catchError((error: any) => throwError(error)));
  }
}
