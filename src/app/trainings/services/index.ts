import {ExerciseService} from './exercise.service';
import {WorkoutService} from './workout.service';
import {UserService} from './user.service';

export const services: any[] = [ExerciseService, WorkoutService, UserService];

export * from '../../share/services/workout-program.service';
export * from './workout.service';
export * from './user.service';
