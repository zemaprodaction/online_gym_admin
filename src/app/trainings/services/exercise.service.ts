import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';

import {Exercise} from '../../share/type/exercise/exercise';
import {TrainingEndpointConstants} from '../../share/constants/endpointConstants';
import {Observable, throwError} from 'rxjs';

@Injectable()
export class ExerciseService {

  constructor(private http: HttpClient) {
  }

  public createExercise(exercise: Exercise): Observable<Exercise> {
    return this.http.post<Exercise>(TrainingEndpointConstants.EXERCISES, exercise)
      .pipe(catchError((error: any) => throwError(error.json())));
  }

  public updateExercise(exercise: Exercise): Observable<Exercise> {
    return this.http.put<Exercise>(TrainingEndpointConstants.EXERCISES, exercise)
      .pipe(catchError((error: any) => throwError(error.json())));
  }

  public getExercises(): Observable<Exercise[]> {
    return this.http.get<Exercise[]>(TrainingEndpointConstants.EXERCISES)
      .pipe(catchError((error: any) => throwError(error.json())));
  }

  public getExercise(id: String): Observable<Exercise> {
    return this.http.get<Exercise>(`/training-service/exercise/${id}`)
      .pipe(catchError((error: any) => throwError(error.json())));
  }

  public removeExercise(ex: Exercise): Observable<string> {
    return this.http.delete<string>(`/workout-program/exercise/${ex.id}`)
      .pipe(catchError((error: any) => {
        return throwError(error.json());
      }));
  }

}
