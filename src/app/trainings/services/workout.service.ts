import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Workout} from '../models/workout/workout';
import * as fromConstants from '../../share/constants/endpointConstants';
import {catchError} from 'rxjs/operators';

@Injectable()
export class WorkoutService {
  constructor(private http: HttpClient) {
  }

  public getWorkoutPrograms(): Observable<Workout[]> {
    return this.http.get<Workout[]>(`${fromConstants.TrainingEndpointConstants.WORKOUT}`)
    .pipe(catchError((error: any) => throwError(error)));
  }

  public removeWorkout(workout: Workout): Observable<string> {
    return this.http.delete<string>(`${fromConstants.TrainingEndpointConstants.WORKOUT}/${workout.id}`)
      .pipe(catchError((error: any) => {
        return throwError(error.json())
      }));
  }

  createWorkout(workout: Workout) {
    return this.http.post<Workout>(fromConstants.TrainingEndpointConstants.WORKOUT, workout)
      .pipe(catchError((error: any) => throwError(error.json())));
  }

  updateWorkout(workout: Workout) {
    return this.http.put<Workout>(fromConstants.TrainingEndpointConstants.WORKOUT, workout)
      .pipe(catchError((error: any) => throwError(error.json())));
  }

}
