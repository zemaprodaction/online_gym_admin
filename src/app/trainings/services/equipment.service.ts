import {Injectable} from '@angular/core';
import {Equipment} from '../../share/type/equipment';

@Injectable()
export class EquipmentService {

  public getSimpleStringEquipment(equipments: Equipment[]) {
    return equipments.join(', ');
  }
}
