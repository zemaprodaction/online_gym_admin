import { WorkoutProgramExistsGuard } from './workout-program-exists-guard.service';
import { WorkoutProgramsGuard } from './workout-programs-guard.service';
import {WorkoutExistsGuard} from './workout-exists.guard';
import {ExerciseExistsGuard} from './exercise-exists.guard';

export const guards: any[] = [WorkoutProgramsGuard, WorkoutProgramExistsGuard, WorkoutExistsGuard,
ExerciseExistsGuard];

export * from './workout-programs-guard.service';
export * from './workout-program-exists-guard.service';
export * from './workout-exists.guard';
export * from './exercise-exists.guard';
