import {Observable} from 'rxjs';
import {filter, map, take, tap} from 'rxjs/operators';
import {WorkoutProgram} from '../models/workout-programs';
import {Workout} from '../models/workout/workout';
import {Store} from '@ngrx/store';
import * as fromStore from '../store';
import {Injectable} from '@angular/core';

@Injectable()
export class TrainingStateUtil {

  constructor(private store: Store<fromStore.TrainingState>) {
  }

  public checkStore(select, dispatchAction): Observable<boolean> {
    return this.store.select(select).pipe(
      tap(loaded => {
        if (!loaded) {
          this.store.dispatch(dispatchAction);
        }
      }),
      filter(loaded => !!loaded),
      take(1)
    );
  }

  public hasEntityInState(id: string, select): Observable<boolean> {
    return this.store.select(select)
    .pipe(
      map((entities: { [key: string]: WorkoutProgram | Workout }) => !!entities[id]),
      take(1)
    );
  }
}
