import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {Observable} from 'rxjs';

import {Store} from '@ngrx/store';

import {switchMap} from 'rxjs/operators';

import * as fromStore from '../store';
import {TrainingStateUtil} from './training-state.util';


@Injectable()
export class WorkoutProgramExistsGuard implements CanActivate {
    constructor(private store: Store<fromStore.TrainingState>, private util: TrainingStateUtil) {
    }

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
        return this.util.checkStore(fromStore.getWorkoutProgramsLoaded,
          fromStore.loadWorkoutPrograms()).pipe(
            switchMap(() => {
                const id = route.params.workoutProgramId;
              return this.util.hasEntityInState(id, fromStore.getWorkoutProgramEntities);
            })
        );
    }
}
