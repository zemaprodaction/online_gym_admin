import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {Observable} from 'rxjs';
import * as fromStore from '../store';
import {Store} from '@ngrx/store';
import {switchMap} from 'rxjs/operators';
import {TrainingStateUtil} from './training-state.util';

@Injectable()
export class WorkoutExistsGuard implements CanActivate {

  constructor(private store: Store<fromStore.TrainingState>, private util: TrainingStateUtil) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.util.checkStore(fromStore.getWorkoutsLoaded,
      fromStore.loadWorkouts()).pipe(
      switchMap(() => {
        const id = route.params.workoutId;
        return this.util.hasEntityInState(id, fromStore.getWorkoutEntities);
      })
    );
  }
}
