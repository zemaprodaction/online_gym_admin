import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import * as fromStore from '../store';
import {TrainingStateUtil} from './training-state.util';
import {switchMap} from 'rxjs/operators';

@Injectable()
export class ExerciseExistsGuard implements CanActivate {
  constructor(private store: Store<fromStore.TrainingState>, private util: TrainingStateUtil) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.util.checkStore(fromStore.getExercisesLoaded,
      fromStore.loadExercises())
      .pipe(
      switchMap(() => {
        const id = route.params.exerciseId;
        return this.util.hasEntityInState(id, fromStore.getExerciseEntities);
      })
    );
  }
}
