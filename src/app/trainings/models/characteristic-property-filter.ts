export interface CharacteristicPropertyFilter {
  name: string;
  id: string;
  values: string[];
  nativeValues: string[];
  filterPath: string;
  editable: boolean;
}
