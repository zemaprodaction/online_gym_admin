export interface CharacteristicProperty {
  name: string;
  id: string;
  value: string;
  nativeValue: string;
}

export interface CharacteristicPropertyList {
  name?: string;
  id?: string;
  value: string[];
  nativeValue: string[];
}
