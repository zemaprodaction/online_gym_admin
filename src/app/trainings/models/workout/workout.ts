import {CommonTrainingInfo} from '../сommonTrainingInfo';
import {WorkoutPhase} from './workout-phase';
import {WorkoutProgramCharacteristic} from '../workout-programs';

export class Workout {
  id: string;
  common: CommonTrainingInfo;
  listOfPhases: WorkoutPhase[];
  duration = 0;
  characteristic?: WorkoutProgramCharacteristic = {
    workoutPlaces: {
      value: [],
      nativeValue: []
    }
  };
}




