import {WorkoutSet} from './workout-set';

export class WorkoutInstancePhase {
  constructor(public name: string, public listOfSets: WorkoutSet[], public isOptional: boolean) {
  }
}

