import {Measure} from '../measure';

export class WorkoutSet {
  constructor(public exerciseId: string) {
  }
  id: number;
  listOfMeasures: Measure[];
  restTime: number;
}
