import {Exercise} from '../../../share/type/exercise/exercise';
import {WorkoutSet} from './workout-set';

export class WorkoutStep {
  exercise: Exercise;
  stepNumber: number;

}
