import {WorkoutInstancePhase} from './workout-instance-phase';

export class WorkoutInstance {
  id: string;
  name: string;
  workoutId: string;
  workoutProgramId: string;
  listOfWorkoutPhases: WorkoutInstancePhase[];
}
