export class WorkoutPhase {
  listOfExercises: string[];
  name: string;
  isOptional: boolean;
}
