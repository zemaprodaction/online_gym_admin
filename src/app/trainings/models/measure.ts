import {MeasureType} from './measureType';

export class Measure {
  expectedValue: number;
  measureTypeId: string;
  measureType: MeasureType;
}
