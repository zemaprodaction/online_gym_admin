import {ImageBundle} from '../../share/type/exercise/exercise';
import {WorkoutProperty} from '../../share/type/workout/workout-property';

export class CommonTrainingInfo {
  name: string;
  description: string;
  imageBundle: ImageBundle;
  workoutType: WorkoutProperty;
  equipmentNecessity: WorkoutProperty;
  workoutDifficulty: WorkoutProperty;
  workoutPlace: WorkoutProperty[];
}
