import {CommonTrainingInfo} from './сommonTrainingInfo';
import {WorkoutInstance} from './workout/workout-instance';
import {Goal} from '../../share/type/goal';
import {CharacteristicProperty, CharacteristicPropertyList} from './characteristic-property';

export interface WorkoutProgramCharacteristic {
  workoutType?: CharacteristicProperty;
  workoutDifficulty?: CharacteristicProperty;
  equipmentNecessity?: CharacteristicProperty;
  workoutPlaces?: CharacteristicPropertyList;
}

export interface WorkoutProgram {
  id?: string;
  common?: CommonTrainingInfo;
  listOfWeeks?: WorkoutWeek[];
  goal?: Goal;
  characteristic?: WorkoutProgramCharacteristic;
  isPublished?: boolean;
}

export class WorkoutWeek {
  constructor(public listOfDays: WorkoutDay[] = []) {
  }

  weekNumber?: number;
}

export interface WorkoutDay {
  listOfScheduledWorkouts?: WorkoutInstance[];
  dayIndex?: number;
}





