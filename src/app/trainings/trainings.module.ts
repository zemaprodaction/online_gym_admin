import {EffectsModule} from '@ngrx/effects';
import {NgModule} from '@angular/core';

import {CommonModule} from '@angular/common';

import {StoreModule} from '@ngrx/store';

import {effects, reducers} from './store';

// services
import * as fromServices from './services';

import * as fromGuards from './guards';

import {MaterialModule} from '../material/material.module';
import {TrainingStateUtil} from './guards/training-state.util';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature('trainings', reducers),
    EffectsModule.forFeature(effects),
    MaterialModule,
  ],
  providers: [...fromServices.services, ...fromGuards.guards, TrainingStateUtil],
  exports: []
})
export class TrainingsModule {
}
