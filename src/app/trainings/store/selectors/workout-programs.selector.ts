import {createSelector} from '@ngrx/store';

import * as fromRoot from '../../../store';
import * as fromFeature from '../reducers';
import * as fromWorkoutPrograms from '../reducers/workout-programs.reducer';
import {WorkoutProgram} from '../../models/workout-programs';


export const getWorkoutProgramsState = createSelector(
  fromFeature.getTrainingsState,
  (state: fromFeature.TrainingState) => state.workoutPrograms
);

export const getWorkoutProgramEntities = createSelector(
  getWorkoutProgramsState,
  fromWorkoutPrograms.getWorkoutProgramsEntities);

export const getSelectedWorkoutProgram = createSelector(
  getWorkoutProgramEntities,
  fromRoot.getRouterState,
  (entities, router): WorkoutProgram => {
      return entities[router.state.params.workoutProgramId];
  }
);

export const getAllWorkoutPrograms = fromWorkoutPrograms.adapter.getSelectors(getWorkoutProgramsState).selectAll;

export const getWorkoutProgramsLoaded = createSelector(
  getWorkoutProgramsState,
  fromWorkoutPrograms.getWorkoutProgramsLoaded
);

export const getWorkoutProgramsLoading = createSelector(
  getWorkoutProgramsState,
  fromWorkoutPrograms.getWorkoutProgramsLoading
);

export const getWorkoutProgramByID = createSelector(
  getWorkoutProgramEntities,
  (entities, props) => entities[props.id]
);
