import {createSelector} from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromUsers from '../reducers/user.reducer';

export const getUserState = createSelector(
  fromFeature.getTrainingsState,
  (state: fromFeature.TrainingState) => state.users
);

export const getUserEntities = createSelector(
  getUserState,
  fromUsers.getUsersEntities);

export const getAllUsers = fromUsers.adapter.getSelectors(getUserState).selectAll;
