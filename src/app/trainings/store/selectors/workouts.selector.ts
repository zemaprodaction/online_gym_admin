import {createSelector} from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromWorkouts from '../reducers/workout.reducer';
import * as fromRoot from '../../../store';
import {Workout} from '../../models/workout/workout';

export const getWorkoutState = createSelector(
  fromFeature.getTrainingsState,
  (state: fromFeature.TrainingState) => state.workouts
);

export const getWorkoutEntities = createSelector(
  getWorkoutState,
  fromWorkouts.getWorkoutsEntities);

export const getSelectedWorkout = createSelector(
  getWorkoutEntities,
  fromRoot.getRouterState,
  (entities, router): Workout => {
    return router.state && entities[router.state.params.workoutId];
  }
);

export const getAllWorkouts = fromWorkouts.adapter.getSelectors(getWorkoutState).selectAll;

export const getWorkoutsLoaded = createSelector(
  getWorkoutState,
  fromWorkouts.getWorkoutsLoaded
);

export const getWorkoutsLoading = createSelector(
  getWorkoutState,
  fromWorkouts.getWorkoutsLoading
);

export const geWorkoutByID = createSelector(
  getWorkoutEntities,
  (entities, props) => entities[props.id]
);
