import {createSelector} from '@ngrx/store';
import * as fromFeature from '../reducers';
import * as fromMeasureType from '../reducers/measure-type.reducer';

export const getMeasureTypesState = createSelector(
  fromFeature.getTrainingsState,
  (state: fromFeature.TrainingState) => state.measureType
);

export const getMeasureTypeEntities = createSelector(
  getMeasureTypesState,
  fromMeasureType.getMeasureTypeEntities
);


export const getAllMeasureType = fromMeasureType.adapter.getSelectors(getMeasureTypesState).selectAll;

export const getMeasureTypeLoaded = createSelector(
  getMeasureTypesState,
  fromMeasureType.getMeasureTypeLoaded
);
export const getMeasureTypeLoading = createSelector(
  getMeasureTypesState,
  fromMeasureType.getMeasureTypeLoading
);
