import {createSelector} from "@ngrx/store";
import * as fromFeature from '../reducers';
import * as fromReducer from '../reducers/workout-instance.reducer';
import * as fromRoot from '../../../store';
import {TrainingParamConstants} from "../../../share/constants/endpointConstants";
import {WorkoutInstance} from "../../models/workout/workout-instance";

export const getWorkoutInstance = createSelector(
  fromFeature.getTrainingsState,
  (state: fromFeature.TrainingState) => state.workoutInstance
);

export const getWorkoutInstanceEntities = createSelector(
  getWorkoutInstance,
  fromReducer.getWorkoutInstanceEntities
);

export const getAllWorkoutInstances = fromReducer.adapter.getSelectors(getWorkoutInstance).selectAll;

export const getSelectedWorkoutInstance = createSelector(
  getWorkoutInstanceEntities,
  fromRoot.getRouterState,
  (entities, router): WorkoutInstance => {
    return entities[router.state.params[TrainingParamConstants.WORKOUT_INSTANCE_ID]];
  }
);

export const getWorkoutInstanceLoaded = createSelector(
  getWorkoutInstance,
  fromReducer.getWorkoutInstanceLoaded
);
export const getWorkoutInstanceLoading = createSelector(
  getWorkoutInstance,
  fromReducer.getWorkoutInstanceLoading
);

export const getSelectWorkoutInstanceByID = createSelector(
  getWorkoutInstance,
  (entities, props) =>  entities[props.id]
);
