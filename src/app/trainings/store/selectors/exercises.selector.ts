import {createSelector} from '@ngrx/store';
import * as fromFeature from '../reducers';
import * as fromExercises from '../reducers/exercises.reducer';
import * as fromRoot from '../../../store';
import {Exercise} from '../../../share/type/exercise/exercise';
import {TrainingParamConstants} from '../../../share/constants/endpointConstants';

export const getExercisesState = createSelector(
  fromFeature.getTrainingsState,
  (state: fromFeature.TrainingState) => state.exercises
);

export const getExerciseEntities = createSelector(
  getExercisesState,
  fromExercises.getExercisesEntities
);

export const getExercises = createSelector(
  getExerciseEntities,
  fromRoot.getRouterState,
  (entities, router): Exercise => {
    return router.state && entities[router.state.params[TrainingParamConstants.EXERCISE_ID]];
  }
);

export const getAllExercises = fromExercises.adapter.getSelectors(getExercisesState).selectAll;

export const getExercisesLoaded = createSelector(
  getExercisesState,
  fromExercises.getExercisesLoaded
);
export const getExercisesLoading = createSelector(
  getExercisesState,
  fromExercises.getExercisesLoading
);

export const getSelectExerciseByID = createSelector(
  getExerciseEntities,
  (entities, props) =>  entities[props.id]
);
export const getSelectExerciseByListID = createSelector(
  getExerciseEntities,
  (entities, props) =>  props.ids.map(id => entities[id])
);
