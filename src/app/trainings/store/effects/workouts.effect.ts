import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import * as workoutActions from '../actions/workouts.action';
import {catchError, map, mergeMap, switchMap} from 'rxjs/operators';
import * as fromServices from '../../services';
import {of} from 'rxjs';
import {RxjsCustomPipeService} from '../../../share/services/rxjs-custome-pipe';

@Injectable()
export class WorkoutsEffect {
  constructor(private action$: Actions,
              private workoutService: fromServices.WorkoutService,
              private pipeService: RxjsCustomPipeService) {
  }

  loadWorkouts$ = createEffect(() =>
    this.action$.pipe(
      ofType(workoutActions.loadWorkouts),
      switchMap(() => {
        return this.workoutService.getWorkoutPrograms().pipe(
          map(workouts => workoutActions.loadWorkoutsSuccess({workouts})),
          catchError(error => of(workoutActions.loadWorkoutsFail(error)))
        );
      })
    )
  );

  deleteWorkout$ = createEffect(() =>
    this.action$.pipe(
      ofType(workoutActions.deleteWorkout),
      this.pipeService.askIfYouWantRemovePipe()(),
      mergeMap((dialogResponse) => {
        const workout = dialogResponse['value'].workout;
        return this.workoutService.removeWorkout(workout).pipe(
          map(() => workoutActions.deleteWorkoutSuccess(workout)),
          catchError(error => of(workoutActions.deleteWorkoutFail(error))
          )
        );
      })
    )
  );

  createWorkout$ = createEffect(() =>
    this.action$.pipe(
      ofType(workoutActions.createWorkout),
      mergeMap(({workout}) => {
        return this.workoutService.createWorkout(workout).pipe(
          map(() => workoutActions.createWorkoutSuccess({workout})),
          catchError(error => of(workoutActions.createWorkoutFail(error)))
        );
      })
    )
  );

  updateExercise$ = createEffect(() =>
    this.action$.pipe(
      ofType(workoutActions.updateWorkout),
      switchMap(({workout}) => {
        return this.workoutService.updateWorkout(workout).pipe(
          map(() => workoutActions.updateWorkoutSuccess({workout: {id: workout.id, changes: {...workout}}}),
            catchError(error => of(workoutActions.updateWorkoutFail(error))
            )
          ));
      })
    )
  );
}
