import {Injectable} from '@angular/core';

import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, map, mergeMap, switchMap} from 'rxjs/operators';

import * as workoutProgramsActions from '../actions/workout-programs.action';
import * as fromServices from '../../../share';
import {of} from 'rxjs';
import {RxjsCustomPipeService} from '../../../share/services/rxjs-custome-pipe';

@Injectable()
export class WorkoutProgramsEffect {

  constructor(private actions$: Actions,
              private workoutProgramService: fromServices.WorkoutProgramService,
              private pipeService: RxjsCustomPipeService) {
  }

  loadWorkoutProgram$ = createEffect(() =>
    this.actions$.pipe(
      ofType(workoutProgramsActions.loadWorkoutPrograms),
      switchMap(() => {
        return this.workoutProgramService.getWorkoutPrograms().pipe(
          map(workoutPrograms => {
            return workoutProgramsActions.loadWorkoutProgramsSuccess({workoutPrograms});
          }),
          catchError(error => of(workoutProgramsActions.loadWorkoutProgramsFail({error})))
        );
      })
    ));

  deleteWorkoutProgram = createEffect(() =>
    this.actions$.pipe(
      ofType(workoutProgramsActions.deleteWorkoutPrograms),
      this.pipeService.askIfYouWantRemovePipe()(),
      switchMap((dialogResponse) => {
        const workoutProgram = dialogResponse['value'].workoutProgram;
        return this.workoutProgramService.removeWorkoutProgram(workoutProgram).pipe(
          map(() => workoutProgramsActions.deleteWorkoutProgramsSuccess({workoutProgram})),
          catchError(error => of(workoutProgramsActions.deleteWorkoutProgramsFail({error}))
          )
        );
      })
    )
  );

  createWorkoutProgram$ = createEffect(() =>
    this.actions$.pipe(
      ofType(workoutProgramsActions.createWorkoutPrograms),
      mergeMap(({workoutProgram}) => {
        return this.workoutProgramService.createWorkoutProgram(workoutProgram).pipe(
          map(() => workoutProgramsActions.createWorkoutProgramsSuccess({workoutProgram})),
          catchError(error => of(workoutProgramsActions.createWorkoutProgramsFail(error))));
      })
    )
  );
  updateWorkoutProgram$ = createEffect(() =>
    this.actions$.pipe(
      ofType(workoutProgramsActions.updateWorkoutPrograms),
      mergeMap(({workoutProgram}) => {
        return this.workoutProgramService.updateWorkoutProgram(workoutProgram).pipe(
          map(() => workoutProgramsActions.updateWorkoutProgramsSuccess({
            workoutProgram: {
              id: workoutProgram.id,
              changes: {...workoutProgram}
            }
          })),
          catchError(error => of(workoutProgramsActions.createWorkoutProgramsFail(error))));
      })
    )
  );
}
