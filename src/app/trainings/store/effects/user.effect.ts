import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import * as userActions from '../actions/user.action';
import {map, switchMap} from 'rxjs/operators';
import * as fromServices from '../../services';

@Injectable()
export class UserEffect {
  constructor(private action$: Actions,
              private userService: fromServices.UserService) {
  }

  loadWorkouts$ = createEffect(() =>
    this.action$.pipe(
      ofType(userActions.loadUsers),
      switchMap(() => {
        return this.userService.getUsers().pipe(
          map(users => userActions.loadUsersSuccess({users})),
        );
      })
    )
  );
}
