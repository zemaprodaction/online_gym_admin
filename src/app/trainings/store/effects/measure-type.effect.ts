import {Injectable} from '@angular/core';
import {of} from 'rxjs';
import {Actions, createEffect, ofType} from '@ngrx/effects';

import * as measureTypeActionActions from '../actions/measure-type.action';
import {catchError, map, switchMap} from 'rxjs/operators';
import {MetadataService} from '../../../share/services/metadata.service';

@Injectable()
export class MeasureTypeEffects {
  constructor(
    private actions$: Actions,
    private metadataService: MetadataService,
  ) {
  }

  loadMeasureType$ = createEffect(() =>
    this.actions$.pipe(
      ofType(measureTypeActionActions.loadMeasureTypes),
      switchMap(() => {
        return this.metadataService.getMeasureTypes().pipe(
          map(measureTypes => measureTypeActionActions.loadMeasureTypesSuccess({measureTypes})),
          catchError(error => of(measureTypeActionActions.loadMeasureTypesFail({error})))
        );
      })
    ));

}
