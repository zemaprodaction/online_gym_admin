import {Injectable} from '@angular/core';
import {of} from 'rxjs';
import {Actions, createEffect, ofType} from '@ngrx/effects';

import * as exercisesActions from '../actions/exercise.action';
import * as fromServices from '../../services/exercise.service';
import {catchError, map, mergeMap, switchMap} from 'rxjs/operators';
import {RxjsCustomPipeService} from '../../../share/services/rxjs-custome-pipe';

@Injectable()
export class ExercisesEffects {
  constructor(
    private actions$: Actions,
    private exercisesService: fromServices.ExerciseService,
    private pipeService: RxjsCustomPipeService
  ) {
  }

  loadExercise$ = createEffect(() =>
    this.actions$.pipe(
      ofType(exercisesActions.loadExercises),
      switchMap(() => {
        return this.exercisesService.getExercises().pipe(
          map(exercises => exercisesActions.loadExercisesSuccess({exercises})),
          catchError(error => of(exercisesActions.loadExercisesFail({error})))
        );
      })
    ));

  deleteExercise$ = createEffect(() =>
    this.actions$.pipe(
      ofType(exercisesActions.deleteExercise),
      this.pipeService.askIfYouWantRemovePipe()(),
      mergeMap((dialogResponse) => {
        const exercise = dialogResponse['value'].exercise;
        return this.exercisesService.removeExercise(exercise).pipe(
          map(() => exercisesActions.deleteExerciseSuccess({exercise})),
          catchError(error => of(exercisesActions.deleteExerciseFail({error}))
          )
        );
      })
    )
  );

  createExercise$ = createEffect(() =>
    this.actions$.pipe(
      ofType(exercisesActions.createExercise),
      mergeMap(({exercise}) => {
        return this.exercisesService.createExercise(exercise).pipe(
          map(() => exercisesActions.createExerciseSuccess({exercise})),
          catchError(error => of(exercisesActions.createExerciseFail(error))));
      })
    )
  );
  updateExercise$ = createEffect(() =>
    this.actions$.pipe(
      ofType(exercisesActions.updateExercise),
      mergeMap(({exercise}) => {
        return this.exercisesService.updateExercise(exercise).pipe(
          map(() => exercisesActions.updateExerciseSuccess({exercise: {id: exercise.id, changes: {...exercise}}})),
          catchError(error => of(exercisesActions.createExerciseFail(error))));
      })
    )
  );
}
