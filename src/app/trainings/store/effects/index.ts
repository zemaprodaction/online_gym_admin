import {WorkoutProgramsEffect} from './workout-programs.effect';
import {ExercisesEffects} from './exerceses.effect';
import {WorkoutsEffect} from './workouts.effect';
import {WorkoutInstanceEffects} from './workout-instance.effects';
import {MeasureTypeEffects} from './measure-type.effect';
import {UserEffect} from './user.effect';

export const effects: any[] = [WorkoutProgramsEffect, ExercisesEffects, WorkoutsEffect,
  WorkoutInstanceEffects, MeasureTypeEffects, UserEffect];

