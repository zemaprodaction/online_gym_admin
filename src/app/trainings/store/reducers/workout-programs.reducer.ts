import * as fromWorkoutsPrograms from '../actions/workout-programs.action';
import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {WorkoutProgram} from '../../models/workout-programs';
import {createReducer, on} from "@ngrx/store";


export interface WorkoutProgramState extends EntityState<WorkoutProgram> {
  loaded: boolean;
  loading: boolean;
}

export const adapter: EntityAdapter<WorkoutProgram> = createEntityAdapter<WorkoutProgram>();

export const initialWorkoutProgramState: WorkoutProgramState = adapter.getInitialState({
  loaded: false,
  loading: false
});

export const reducer = createReducer(
  initialWorkoutProgramState,
  on(fromWorkoutsPrograms.loadWorkoutPrograms, state => ({
    ...state,
    loading: true
  })),
  on(fromWorkoutsPrograms.loadWorkoutProgramsSuccess, (state, {workoutPrograms}) => (
    adapter.addAll(workoutPrograms, {...state, loading: false, loaded: true})
  )),
  on(fromWorkoutsPrograms.loadWorkoutProgramsFail, state => ({...state, loaded: false, loading: false})),
  on(fromWorkoutsPrograms.deleteWorkoutProgramsSuccess, (state, {workoutProgram}) => (
    adapter.removeOne(workoutProgram.id, {...state})
  )),
  on(fromWorkoutsPrograms.createWorkoutProgramsSuccess, (state, {workoutProgram}) => (
    adapter.addOne(workoutProgram, {...state})
  )),
  on(fromWorkoutsPrograms.updateWorkoutProgramsSuccess, (state, {workoutProgram}) => (
    adapter.updateOne(workoutProgram, {...state})
  ))
);

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal

} = adapter.getSelectors();

export const getWorkoutProgramsLoading = (state: WorkoutProgramState) => state.loading;
export const getWorkoutProgramsLoaded = (state: WorkoutProgramState) => state.loaded;
export const getWorkoutProgramsEntities = (state: WorkoutProgramState) => state.entities;
