import {createReducer, on} from '@ngrx/store';
import {createEntityAdapter, EntityAdapter, EntityState} from "@ngrx/entity";
import {WorkoutInstance} from "../../models/workout/workout-instance";
import * as fromAction from "../actions/workout-instance.actions";

export interface WorkoutInstanceState extends  EntityState<WorkoutInstance> {
  loaded: boolean;
  loading: boolean;
}

export const adapter: EntityAdapter<WorkoutInstance> = createEntityAdapter();

export const initialWorkoutInstanceState: WorkoutInstanceState = adapter.getInitialState({
  loaded: false,
  loading: false,
});

export const reducer = createReducer(
  initialWorkoutInstanceState,
  on(fromAction.loadSelectedWorkoutInstanceSuccess, (state, {workoutInstances}) => (
    adapter.addOne(workoutInstances, {...state}))
  ),
  on(fromAction.loadWorkoutInstance, state => (
    {
      ...state,
      loading: true
    }
  )),
  on(fromAction.loadWorkoutInstanceFail, state => ({...state, loaded: false, loading: false})),
  on(fromAction.loadWorkoutInstanceSuccess, (state, {workoutInstances}) => (
    adapter.addOne(workoutInstances, {...state})
  )),
  on(fromAction.workoutInstanceFinishedSuccess, (state, {workoutInstances}) => (
    adapter.updateOne(workoutInstances, {...state})
  )),
  on(fromAction.loadWorkoutInstanceList, (state, {workoutInstanceList}) => (
    adapter.addAll(workoutInstanceList, {...state, loading: false})
  ))
);

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal

} = adapter.getSelectors();

export const getWorkoutInstanceLoading = (state: WorkoutInstanceState) => state.loading;
export const getWorkoutInstanceLoaded = (state: WorkoutInstanceState) => state.loaded;
export const getWorkoutInstanceEntities = (state: WorkoutInstanceState) => state.entities;

