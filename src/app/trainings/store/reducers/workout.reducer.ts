import * as fromWorkouts from '../actions/workouts.action';
import {Workout} from '../../models/workout/workout';
import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {createReducer, on} from '@ngrx/store';


export interface WorkoutState extends EntityState<Workout> {
  loaded: boolean;
  loading: boolean;
}

export const adapter: EntityAdapter<Workout> = createEntityAdapter<Workout>();

export const initialWorkoutState: WorkoutState = adapter.getInitialState({
  loaded: false,
  loading: false
});

export const reducer = createReducer(
  initialWorkoutState,
  on(fromWorkouts.loadWorkouts, state => ({
    ...state,
    loading: true
  })),
  on(fromWorkouts.loadWorkoutsSuccess, (state, {workouts}) => (
    adapter.addAll(workouts, {...state, loading: false, loaded: true})
  )),
  on(fromWorkouts.loadWorkoutsFail, state => ({...state, loaded: false, loading: false})),
  on(fromWorkouts.deleteWorkoutSuccess, (state, {workout}) => (
    adapter.removeOne(workout.id, {...state})
  )),
  on(fromWorkouts.createWorkoutSuccess, (state, {workout}) => (
    adapter.addOne(workout, {...state})
  )),
  on(fromWorkouts.updateWorkoutSuccess, (state, {workout}) => (
    adapter.updateOne(workout, {...state})
  ))
);

export const getWorkoutsEntities = (state: WorkoutState) => state.entities;
export const getWorkoutsLoaded = (state: WorkoutState) => state.loaded;
export const getWorkoutsLoading = (state: WorkoutState) => state.loading;

