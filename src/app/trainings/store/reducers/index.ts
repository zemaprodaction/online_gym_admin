import {ActionReducerMap, createFeatureSelector,} from '@ngrx/store';

import * as fromWorkoutPrograms from './workout-programs.reducer';
import * as fromExercises from './exercises.reducer';
import * as fromWorkouts from './workout.reducer';
import * as fromWorkoutInstance from './workout-instance.reducer';
import * as fromMeasureType from './measure-type.reducer';
import * as fromUsers from './user.reducer';

export interface TrainingState {
  workoutPrograms: fromWorkoutPrograms.WorkoutProgramState;
  workouts: fromWorkouts.WorkoutState;
  exercises: fromExercises.ExerciseState;
  workoutInstance: fromWorkoutInstance.WorkoutInstanceState;
  measureType: fromMeasureType.MeasureTypeState;
  users: fromUsers.UserState;
}

export const reducers: ActionReducerMap<TrainingState> = {
  workoutPrograms: fromWorkoutPrograms.reducer,
  workouts: fromWorkouts.reducer,
  exercises: fromExercises.reducer,
  workoutInstance: fromWorkoutInstance.reducer,
  measureType: fromMeasureType.reducer,
  users: fromUsers.reducer
};

export const getTrainingsState = createFeatureSelector<TrainingState>(
  'trainings'
);
