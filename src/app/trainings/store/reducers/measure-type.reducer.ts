import * as fromMeasureType from '../actions/measure-type.action';
import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {createReducer, on} from '@ngrx/store';
import {MeasureType} from '../../models/measureType';

export interface MeasureTypeState extends EntityState<MeasureType> {
  loaded: boolean;
  loading: boolean;
}

export const adapter: EntityAdapter<MeasureType> = createEntityAdapter<MeasureType>({
  sortComparer: false
});

export const initialExerciseState: MeasureTypeState = adapter.getInitialState({
  loaded: false,
  loading: false
});

export const reducer = createReducer(
  initialExerciseState,
  on(fromMeasureType.loadMeasureTypes, state => ({
    ...state,
    loading: true
  })),
  on(fromMeasureType.loadMeasureTypesSuccess, (state, {measureTypes}) => (
    adapter.addMany(measureTypes, {...state, loading: false, loaded: true})
  )),
  on(fromMeasureType.loadMeasureTypesFail, state => ({...state, loaded: false, loading: false})));


export const getMeasureTypeEntities = (state: MeasureTypeState) => state.entities;
export const getMeasureTypeLoaded = (state: MeasureTypeState) => state.loaded;
export const getMeasureTypeLoading = (state: MeasureTypeState) => state.loading;

