import * as fromUsers from '../actions/user.action';
import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {createReducer, on} from '@ngrx/store';
import {UserKeycloak} from '../../models/user-keycloak';


export interface UserState extends EntityState<UserKeycloak> {
  loaded: boolean;
  loading: boolean;
}

export const adapter: EntityAdapter<UserKeycloak> = createEntityAdapter<UserKeycloak>();

export const initialWorkoutState: UserState = adapter.getInitialState({
  loaded: false,
  loading: false
});

export const reducer = createReducer(
  initialWorkoutState,
  on(fromUsers.loadUsers, state => ({
    ...state,
    loading: true
  })),
  on(fromUsers.loadUsersSuccess, (state, {users}) => (
    adapter.addMany(users, {...state, loading: false, loaded: true})
  )),
);

export const getUsersEntities = (state: UserState) => state.entities;
export const getUsersLoaded = (state: UserState) => state.loaded;
export const getUsersLoading = (state: UserState) => state.loading;

