import {Exercise} from '../../../share/type/exercise/exercise';
import * as fromExercise from '../actions/exercise.action';
import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {createReducer, on} from '@ngrx/store';

export interface ExerciseState extends EntityState<Exercise> {
  loaded: boolean;
  loading: boolean;
}

export const adapter: EntityAdapter<Exercise> = createEntityAdapter<Exercise>({
  selectId: (exercise: Exercise) => exercise.id,
  sortComparer: false
});

export const initialExerciseState: ExerciseState = adapter.getInitialState({
  loaded: false,
  loading: false
});

export const reducer = createReducer(
  initialExerciseState,
  on(fromExercise.loadExercises, state => ({
    ...state,
    loading: true
  })),
  on(fromExercise.loadExercisesSuccess, (state, {exercises}) => (
    adapter.addAll(exercises, {...state, loading: false, loaded: true})
  )),
  on(fromExercise.loadExercisesFail, state => ({...state, loaded: false, loading: false})),
  on(fromExercise.deleteExerciseSuccess, (state, {exercise}) => (
    adapter.removeOne(exercise.id, {...state})
  )),
  on(fromExercise.createExerciseSuccess, (state, {exercise}) => {
    return adapter.addOne(exercise, {...state});
    }
  ),
  on(fromExercise.updateExerciseSuccess, (state, {exercise}) => (
    adapter.updateOne(exercise, {...state})
  )));


export const getExercisesEntities = (state: ExerciseState) => state.entities;
export const getExercisesLoaded = (state: ExerciseState) => state.loaded;
export const getExercisesLoading = (state: ExerciseState) => state.loading;

