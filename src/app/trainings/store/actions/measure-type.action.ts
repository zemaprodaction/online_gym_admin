import {createAction, props} from '@ngrx/store';
import {MeasureType} from '../../models/measureType';

export const loadMeasureTypes = createAction(
  '[Training] Load Measures type'
);

export const loadMeasureTypesFail = createAction(
  '[Training] Load Measures type Fail',
  props<{error: any}>()
);

export const loadMeasureTypesSuccess = createAction(
  '[Training] Load Measures type Success',
  props<{measureTypes: MeasureType[]}>()
);




