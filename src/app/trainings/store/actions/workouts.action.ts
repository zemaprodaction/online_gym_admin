import {createAction, props} from '@ngrx/store';
import {Workout} from '../../models/workout/workout';
import {Update} from '@ngrx/entity';

export const loadWorkouts = createAction(
  '[Training] Load Workouts'
);

export const loadWorkoutsFail = createAction(
  '[Training] Load Workouts Fail',
  props<{error: any}>()
);

export const loadWorkoutsSuccess = createAction(
  '[Training] Load Workouts Success',
  props<{workouts: Workout[]}>()
);

export const deleteWorkout = createAction(
  '[Training] Delete Workout',
  props<{workout: Workout}>()
);

export const deleteWorkoutFail = createAction(
  '[Training] Delete Workout Fail',
  props<{error: any}>()
);
export const deleteWorkoutSuccess = createAction(
  '[Training] Delete Workout Success',
  props<{workout: Workout}>()
);

export const createWorkout = createAction(
  '[Training] Workout Exercises',
  props<{workout: Workout}>()
);
export const createWorkoutFail = createAction(
  '[Training] Create Workout Fail',
  props<{error: any}>()
);
export const createWorkoutSuccess = createAction(
  '[Training] Create Workout Success',
  props<{workout: Workout}>()
);

export const updateWorkout = createAction(
  '[Training] Update Workout',
  props<{workout: Workout}>()
);
export const updateWorkoutFail = createAction(
  '[Training] Update Workout Fail',
  props<{error: any}>()
);
export const updateWorkoutSuccess = createAction(
  '[Training] Update Workout Success',
  props<{workout: Update<Workout>}>()
);
