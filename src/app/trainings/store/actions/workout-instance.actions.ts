import {createAction, props} from '@ngrx/store';
import {WorkoutInstance} from "../../models/workout/workout-instance";
import {Update} from "@ngrx/entity";

export const loadSelectedWorkoutInstanceFromWorkoutProgram = createAction(
  '[Training] Load Selected Workout Instance From Workout Program',
);

export const loadSelectedWorkoutInstanceSuccess = createAction(
  '[Training] Load Selected Workout Instance From Workout Program Success',
  props<{workoutInstances: WorkoutInstance}>()
);

export const loadWorkoutInstanceList = createAction(
  '[Training] Load Workout Instance List',
  props<{workoutInstanceList: WorkoutInstance[]}>()
);

export const loadWorkoutInstance = createAction(
  '[Training] Load Workout Instance',
  props<{workoutProgramId: string}>()
);

export const loadWorkoutInstanceFail = createAction(
  '[Training] Load Workout Instance Fail',
  props<{error: any}>()
);

export const loadWorkoutInstanceSuccess = createAction(
  '[Training] Load Workout Instance Success',
  props<{workoutInstances: WorkoutInstance}>()
);
export const workoutInstanceFinished  = createAction(
  '[Training] Workout Instance Finishing',
  props<{workoutInstanceId: string, workoutProgramId: string}>()
);
export const workoutInstanceFinishedSuccess  = createAction(
  '[Training] Workout Instance Finish successful',
  props<{workoutInstances: Update<WorkoutInstance>}>()
);

