import {createAction, props} from '@ngrx/store';

import {Exercise} from '../../../share/type/exercise/exercise';
import {Update} from '@ngrx/entity';

export const loadExercises = createAction(
  '[Training] Load Exercises'
);

export const loadExercisesFail = createAction(
  '[Training] Load Exercises Fail',
  props<{error: any}>()
);

export const loadExercisesSuccess = createAction(
  '[Training] Load Exercises Success',
  props<{exercises: Exercise[]}>()
);

export const deleteExercise = createAction(
  '[Training] Delete Exercise',
  props<{exercise: Exercise}>()
);

export const deleteExerciseFail = createAction(
  '[Training] Delete Exercise Fail',
  props<{error: any}>()
);
export const deleteExerciseSuccess = createAction(
  '[Training] Delete Exercise Success',
  props<{exercise: Exercise}>()
);

export const createExercise = createAction(
  '[Training] Create Exercise',
  props<{exercise: Exercise}>()
);
export const createExerciseFail = createAction(
  '[Training] Create Exercise Fail',
  props<{error: any}>()
);
export const createExerciseSuccess = createAction(
  '[Training] Create Exercise Success',
  props<{exercise: Exercise}>()
);

export const updateExercise = createAction(
  '[Training] Update Exercises',
  props<{exercise: Exercise}>()
);
export const updateExerciseFail = createAction(
  '[Training] Update Exercises Fail',
  props<{error: any}>()
);
export const updateExerciseSuccess = createAction(
  '[Training] Update Exercises Success',
  props<{exercise: Update<Exercise>}>()
);



