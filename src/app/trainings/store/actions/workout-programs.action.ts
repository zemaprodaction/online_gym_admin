import {createAction, props} from '@ngrx/store';
import {WorkoutProgram} from "../../models/workout-programs";
import {Update} from "@ngrx/entity";

export const loadWorkoutPrograms = createAction(
  '[Training] Load Workout Programs'
);
export const loadWorkoutProgramsFail = createAction(
  '[Training] Load Workout Programs Fail',
  props<{ error: any }>()
);
export const loadWorkoutProgramsSuccess = createAction(
  '[Training] Load Workout Programs Success',
  props<{ workoutPrograms: WorkoutProgram[] }>()
  );

export const deleteWorkoutPrograms = createAction(
  '[Training] Delete Workout Programs',
  props<{ workoutProgram: WorkoutProgram }>()
);
export const deleteWorkoutProgramsFail = createAction(
  '[Training] Delete Workout Programs Fail',
  props<{ error: any }>()
);
export const deleteWorkoutProgramsSuccess = createAction(
  '[Training] Delete Workout Programs Success',
  props<{ workoutProgram: WorkoutProgram }>()
);

export const createWorkoutPrograms = createAction(
  '[Training] Create Workout Programs',
  props<{ workoutProgram: WorkoutProgram }>()
);
export const createWorkoutProgramsFail = createAction(
  '[Training] Create Workout Programs Fail',
  props<{ error: any }>()
);
export const createWorkoutProgramsSuccess = createAction(
  '[Training] Create Workout Programs Success',
  props<{ workoutProgram: WorkoutProgram }>()
);
export const updateWorkoutPrograms = createAction(
  '[Training] Update Workout Programs',
  props<{ workoutProgram: WorkoutProgram }>()
);
export const updateWorkoutProgramsFail = createAction(
  '[Training] Update Workout Programs Fail',
  props<{ error: any }>()
);
export const updateWorkoutProgramsSuccess = createAction(
  '[Training] Update Workout Programs Success',
  props<{ workoutProgram: Update<WorkoutProgram> }>()
);

