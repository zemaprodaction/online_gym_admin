import {createAction, props} from '@ngrx/store';
import {UserKeycloak} from '../../models/user-keycloak';

export const loadUsers = createAction(
  '[Training] Load Users'
);

export const loadUsersSuccess = createAction(
  '[Training] Load Users Success',
  props<{users: UserKeycloak[]}>()
);

