export const appRoutesNames = {
  TRAINING: 'training',
  ADMIN: 'admin',
  USER: 'user',
  TRAINING_PROCESS: 'training-process'
};
