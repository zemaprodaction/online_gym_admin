import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';

import {CustomSerializer, effects, reducers} from './store';

import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material/material.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {routing} from './app.routing';
import {RouterState, StoreRouterConnectingModule} from '@ngrx/router-store';
import {AuthorizationModule} from './authorization/authorization.module';
import { DialogComponent } from './share/dialog/dialog.component';
import {CsrfInterceptor} from './interceptor/csrf.interceptor';

const environment = {
  development: true,
  production: false,
};

@NgModule({
  declarations: [AppComponent, DialogComponent],
  imports: [
    BrowserModule,
    routing,
    AuthorizationModule,
    StoreRouterConnectingModule.forRoot({
      routerState: RouterState.Minimal,
      serializer: CustomSerializer
    }),
    StoreModule.forRoot(reducers, {
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
        strictStateSerializability: true,
        strictActionSerializability: true,
      }
    }),
    EffectsModule.forRoot(effects),

    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    }),
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: CsrfInterceptor, multi: true},
  ],
  bootstrap: [AppComponent],
  entryComponents: []
})
export class AppModule {
}
