export class User {
  name: string;
  imageUrl: string;
  isAuthorized: boolean;
}
