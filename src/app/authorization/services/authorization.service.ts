import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../model/user";
import * as fromConstants from "../../share/constants/endpointConstants";

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(private http: HttpClient) { }

  public getAuthorization(): Observable<User> {
    return this.http.get<User>(fromConstants.AuthorizationConstants.AUTHORIZATION)
  }
}
