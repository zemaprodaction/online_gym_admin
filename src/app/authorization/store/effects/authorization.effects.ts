import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {AuthorizationService} from "../../services/authorization.service";
import * as authorizationAction from "../actions/authorization.actions";
import {catchError, map, switchMap} from "rxjs/operators";
import {of} from "rxjs";
import {loadWorkoutPrograms} from "../../../trainings/store/actions";


@Injectable()
export class AuthorizationEffects {

  constructor(private actions$: Actions, private authorizationService: AuthorizationService) {
  }

  getAuthorization$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadWorkoutPrograms),
      switchMap(() => {
        return this.authorizationService.getAuthorization().pipe(
          map(authorization => {
            return authorization.isAuthorized ? authorizationAction.authorizationSuccess({user: authorization})
              : authorizationAction.authorizationAnonymous({user: authorization})
          }),
          catchError(error => {
            return of(authorizationAction.authorizationFail(error))
          })
        )
      })
    )
  );

}
