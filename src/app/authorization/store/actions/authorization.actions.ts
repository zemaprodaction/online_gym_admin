import {createAction, props} from '@ngrx/store';
import {User} from "../../model/user";

export const getAuthorization = createAction(
  '[Authorization] Get Authorization'
);

export const authorizationSuccess = createAction(
  '[Authorization] Authorization Success',
  props<{user: User}>()
);
export const authorizationFail = createAction(
  '[Authorization] Authorization Fail',
  props<{error: any}>()
);
export const authorizationAnonymous = createAction(
  '[Authorization] Authorization Anonymous',
  props<{user: User}>()
);
