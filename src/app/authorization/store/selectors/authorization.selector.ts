import {createSelector} from "@ngrx/store";
import * as fromAuthorization from '../reducers/authorization.reducer'
import * as fromFeature from "../reducers";

export const getAuthorizationState = createSelector(
  fromFeature.getAuthorizationState,
  (state: fromFeature.AuthorizationState) => state.authorization
);

export const isAuthorizationUser = createSelector(
  getAuthorizationState,
  fromAuthorization.getIsAuthorization
);
export const selectAuthorizationUser = createSelector(
  getAuthorizationState,
  fromAuthorization.selectAuthorization
);
