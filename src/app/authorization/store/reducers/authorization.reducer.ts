import {Action, createReducer, on} from '@ngrx/store';
import {createEntityAdapter, EntityAdapter} from '@ngrx/entity';
import {User} from '../../model/user';
import {authorizationAnonymous, authorizationSuccess, getAuthorization} from '../actions/authorization.actions';


export interface AuthorizationState {
  isAuthorization: boolean;
  user: User;
}

export const adapter: EntityAdapter<User> = createEntityAdapter<User>();

export const initialAuthorizationState: AuthorizationState = adapter.getInitialState({
  isAuthorization: false,
  user: null
});
const authorizationReducer = createReducer(
  initialAuthorizationState,
  on(getAuthorization, state => ({
    ...state,
  })),
  on(authorizationSuccess, (state, {user}) => (
    {...state, isAuthorization: true, user})
  ),
  on(authorizationAnonymous, (state, {user}) => (
    {...state, isAuthorization: true, user})
  ));

export const getIsAuthorization = (state: AuthorizationState) => {
  return state.isAuthorization;
};
export const selectAuthorization = (state: AuthorizationState) => {
  return state.user;
};
export function reducer(state: AuthorizationState | undefined, action: Action) {
  return authorizationReducer(state, action);
}

