import * as fromAuthorization from './authorization.reducer';
import {ActionReducerMap, createFeatureSelector} from '@ngrx/store';

export const authorizationFeatureKey = 'authorization';

export interface AuthorizationState {
  authorization: fromAuthorization.AuthorizationState;
}

export const reducers: ActionReducerMap<AuthorizationState> = {
  authorization: fromAuthorization.reducer
};
export const getAuthorizationState = createFeatureSelector<AuthorizationState>(
  authorizationFeatureKey
);
