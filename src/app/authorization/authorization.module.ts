import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {authorizationFeatureKey, reducers} from './store/reducers';
import {EffectsModule} from '@ngrx/effects';
import {effects} from './store';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(authorizationFeatureKey, reducers),
    EffectsModule.forFeature(effects),
  ]
})
export class AuthorizationModule { }
