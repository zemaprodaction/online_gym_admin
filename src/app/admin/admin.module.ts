import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import * as fromContainers from './containers';
import {MaterialModule} from '../material/material.module';
import {TrainingsModule} from '../trainings/trainings.module';
import {Routing} from './admin.router';
import {AdminMaterialModule} from '../material/admin/admin.module';
import { UserAdminPageComponent } from './containers/user-admin-page/user-admin-page.component';


@NgModule({
  declarations: [...fromContainers.containers, UserAdminPageComponent],
  imports: [
    Routing,
    CommonModule,
    MaterialModule,
    TrainingsModule,
    AdminMaterialModule
  ]
})
export class AdminModule { }
