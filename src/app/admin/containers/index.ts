import {ExerciseAdminPageComponent} from './exercise-admin-page/exercise-admin-page.component';
import {WorkoutAdminPageComponent} from './workout-admin-page/workout-admin-page.component';
import {WorkoutProgramAdminPageComponent} from './workout-program-admin-page/workout-program-admin-page.component';
import {AdminPageComponent} from './admin-page/admin-page.component';
import {UserAdminPageComponent} from './user-admin-page/user-admin-page.component';


export const containers = [ExerciseAdminPageComponent, WorkoutAdminPageComponent,
  WorkoutProgramAdminPageComponent, AdminPageComponent, UserAdminPageComponent];

export * from './exercise-admin-page/exercise-admin-page.component';
export * from './workout-admin-page/workout-admin-page.component';
export * from './workout-program-admin-page/workout-program-admin-page.component';
export * from './admin-page/admin-page.component';
export * from './user-admin-page/user-admin-page.component';
