import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractAdminPageComponent} from '../abstract-admin-page/abstract-admin-page.component';
import * as fromStore from '../../../trainings/store';
import {Observable} from 'rxjs';
import {Exercise} from '../../../share/type/exercise/exercise';
import {Action, Store} from '@ngrx/store';

@Component({
  selector: 'app-exercise-admin-page',
  templateUrl: './exercise-admin-page.component.html',
  styleUrls: ['./exercise-admin-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExerciseAdminPageComponent extends AbstractAdminPageComponent implements OnInit {

  constructor(protected store: Store<fromStore.TrainingState>, protected changeDetectorRefs: ChangeDetectorRef) {
    super(store, changeDetectorRefs);
  }

  protected initDispatchAction(): Action {
    return fromStore.loadExercises();
  }

  protected selectEntities(): Observable<any[]> {
    return this.store.select(fromStore.getAllExercises) as Observable<Exercise[]>;
  }

  protected initDeleteDispatchAction(data) {
    this.store.dispatch(fromStore.deleteExercise({exercise: data}));
  }

  protected initCreateDispatchAction(data) {
    this.store.dispatch(fromStore.createExercise({exercise: data}));
  }

  protected initUpdateDispatchAction(data) {
    this.store.dispatch(fromStore.updateExercise({exercise: data}));
  }

}
