import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseAdminPageComponent } from './exercise-admin-page.component';

describe('ExerciseAdminPageComponent', () => {
  let component: ExerciseAdminPageComponent;
  let fixture: ComponentFixture<ExerciseAdminPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExerciseAdminPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExerciseAdminPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
