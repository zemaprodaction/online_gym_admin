import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkoutProgramAdminPageComponent } from './workout-program-admin-page.component';

describe('WorkoutProgramAdminPageComponent', () => {
  let component: WorkoutProgramAdminPageComponent;
  let fixture: ComponentFixture<WorkoutProgramAdminPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkoutProgramAdminPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkoutProgramAdminPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
