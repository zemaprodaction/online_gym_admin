import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Action, Store} from '@ngrx/store';
import * as fromStore from '../../../trainings/store';
import {AbstractAdminPageComponent} from '../abstract-admin-page/abstract-admin-page.component';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-workout-program-admin-page',
  templateUrl: './workout-program-admin-page.component.html',
  styleUrls: ['./workout-program-admin-page.component.css']
})
export class WorkoutProgramAdminPageComponent extends AbstractAdminPageComponent implements OnInit {

  constructor(protected store: Store<fromStore.TrainingState>, protected changeDetectorRefs: ChangeDetectorRef) {
    super(store, changeDetectorRefs);
  }

  protected initDispatchAction(): Action {
    return fromStore.loadWorkoutPrograms();
  }

  protected selectEntities(): Observable<any[]> {
    return this.store.select(fromStore.getAllWorkoutPrograms).pipe(
      map(workoutArr => workoutArr.map(workout => {
          return {
            id: workout.id,
            name: workout.common.name,
          };
        })
      ));
  }

  protected initDeleteDispatchAction(data) {
    this.store.dispatch(fromStore.deleteWorkoutPrograms({workoutProgram: data}));
  }

  protected initCreateDispatchAction(data) {
    this.store.dispatch(fromStore.createWorkoutPrograms({workoutProgram: data}));
  }

  protected initUpdateDispatchAction(data) {
    this.store.dispatch(fromStore.updateWorkoutPrograms({workoutProgram: data}));
  }

}
