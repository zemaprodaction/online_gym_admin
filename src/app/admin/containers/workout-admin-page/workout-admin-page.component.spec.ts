import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkoutAdminPageComponent } from './workout-admin-page.component';

describe('WorkoutAdminPageComponent', () => {
  let component: WorkoutAdminPageComponent;
  let fixture: ComponentFixture<WorkoutAdminPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkoutAdminPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkoutAdminPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
