import {ChangeDetectorRef, Component} from '@angular/core';
import {AbstractAdminPageComponent} from '../abstract-admin-page/abstract-admin-page.component';
import {Action, Store} from '@ngrx/store';
import * as fromStore from '../../../trainings/store';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-workout-admin-page',
  templateUrl: './workout-admin-page.component.html',
  styleUrls: ['./workout-admin-page.component.css']
})
export class WorkoutAdminPageComponent extends AbstractAdminPageComponent {

  constructor(protected store: Store<fromStore.TrainingState>, protected changeDetectorRefs: ChangeDetectorRef) {
    super(store, changeDetectorRefs);
  }

  protected initDispatchAction(): Action {
    return fromStore.loadWorkouts();
  }

  protected selectEntities(): Observable<any[]> {
    return this.store.select(fromStore.getAllWorkouts).pipe(
      map(workoutArr => workoutArr.map(workout => {
          const dto = {...workout, ...workout.common};
          delete dto['common'];
          return dto;
        })
      ));
  }

  protected initDeleteDispatchAction(data) {
    this.store.dispatch(fromStore.deleteWorkout({workout: data}));
  }

  protected initCreateDispatchAction(data) {
    this.store.dispatch(fromStore.createWorkout({workout: data}));
  }
  protected initUpdateDispatchAction(data) {
    this.store.dispatch(fromStore.updateWorkout({workout: data}));
  }
}
