import { ChangeDetectorRef, OnDestroy, OnInit, Directive } from '@angular/core';
import {Action, Store} from '@ngrx/store';
import * as fromStore from '../../../trainings/store';
import {Observable, Subscription} from 'rxjs';


@Directive()
export abstract class AbstractAdminPageComponent implements OnDestroy, OnInit {

  subscription: Subscription;
  protected constructor(protected store: Store<fromStore.TrainingState>,
                        protected changeDetectorRefs: ChangeDetectorRef) {
  }

  $entitiesList: Observable<any[]>;

  ngOnInit() {
    this.$entitiesList = this.selectEntities();
    this.store.dispatch(this.initDispatchAction());
    this.subscription = this.$entitiesList.subscribe(() => {
      this.changeDetectorRefs.detectChanges();
    });
    this.store.dispatch(fromStore.loadExercises());
  }

  ngOnDestroy() {
    this.changeDetectorRefs.detach(); // do this
    this.subscription.unsubscribe();
  }

  deleteItem($event: any) {
    this.initDeleteDispatchAction($event);
  }

  addNew($event: any) {
    this.initCreateDispatchAction($event);
  }

  updateItem($event: any) {
    this.initUpdateDispatchAction($event);
  }

  protected abstract initDispatchAction(): Action;

  protected abstract initDeleteDispatchAction(data);

  protected abstract initCreateDispatchAction(data);

  protected abstract initUpdateDispatchAction(data);

  protected abstract selectEntities(): Observable<any[]>;

}
