export const adminRoutesNames = {
  WORKOUT_PROGRAM: 'workout-program-admin',
  WORKOUT: 'workout-admin',
  EXERCISE: 'exercise-admin',
  USER: 'user-admin',
};
