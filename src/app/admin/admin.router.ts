import {RouterModule, Routes} from '@angular/router';
import * as fromContainers from './containers';
import {NgModule} from '@angular/core';
import {adminRoutesNames} from './routes.names';

const routes: Routes = [
  { path: '', component: fromContainers.AdminPageComponent, children: [
      {
        path: `${adminRoutesNames.EXERCISE}`,
        component: fromContainers.ExerciseAdminPageComponent
      },
      {
        path: `${adminRoutesNames.WORKOUT}`,
        component: fromContainers.WorkoutAdminPageComponent
      },
      {
        path: `${adminRoutesNames.WORKOUT_PROGRAM}`,
        component: fromContainers.WorkoutProgramAdminPageComponent
      },
      {
        path: `${adminRoutesNames.USER}`,
        component: fromContainers.UserAdminPageComponent
      }
    ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Routing {
}
