import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CommonModule} from '@angular/common';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {MatNativeDateModule} from '@angular/material/core';
import {MatDialogModule} from '@angular/material/dialog';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatStepperModule} from '@angular/material/stepper';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTableModule} from '@angular/material/table';
import {NavigationComponent} from './navigation/navigation.component';
import {LayoutModule} from '@angular/cdk/layout';
import {ToolbarComponent} from './toolbar/toolbar.component';
import {MatFormFieldModule} from '@angular/material/form-field';

import {CUSTOM_BREAKPOINTS} from './common/custom-breakpoints';

import * as fromDirectives from './directive';

import {RouterModule} from '@angular/router';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import {ReactiveFormsModule} from '@angular/forms';
import {AutocompleteMultiplyFormComponent} from './admin/form-components/autocomplete-multiply-form/autocomplete-multiply-form.component';


@NgModule({
  declarations: [
    ...fromDirectives.directives,
    NavigationComponent, ToolbarComponent],
  imports: [CommonModule, MatButtonModule, MatToolbarModule, MatNativeDateModule, MatIconModule,
    MatSidenavModule, MatListModule, MatMenuModule, MatCardModule,
    FlexLayoutModule.withConfig({disableDefaultBps: false}, CUSTOM_BREAKPOINTS),
    MatGridListModule, LayoutModule, MatChipsModule,
    MatProgressBarModule,
    RouterModule,
    MatStepperModule,
    MatBottomSheetModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatTableModule, MatSlideToggleModule, MatAutocompleteModule, MatTabsModule, MatTooltipModule,

  ],
  exports: [
    MatDialogModule,
    MatTableModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatButtonModule,
    NavigationComponent,
    FlexLayoutModule,
    MatGridListModule,
    MatInputModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatToolbarModule, MatIconModule, ToolbarComponent,
    MatStepperModule, MatListModule, MatSelectModule],
})
export class MaterialModule {
}
