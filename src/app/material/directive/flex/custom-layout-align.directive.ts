/* tslint:disable:use-input-property-decorator */
import {Directive} from '@angular/core';
import {LayoutAlignDirective} from '@angular/flex-layout';

const selector = `[fxLayoutAlign], [fxLayoutAlign.mobile],
                  [fxLayoutAlign.sTablet], [fxLayoutAlign.lTablet],
                  [fxLayoutAlign.lTabletLandscape], [fxLayoutAlign.web]
`;

const inputs = [
  'fxLayoutAlign',
  'fxLayoutAlign.mobile',
  'fxLayoutAlign.sTablet',
  'fxLayoutAlign.lTablet',
  'fxLayoutAlign.lTabletLandscape',
  'fxLayoutAlign.web'
];

@Directive({selector, inputs})
export class CustomLayoutAlignDirective extends LayoutAlignDirective {
  protected inputs = inputs;
}
