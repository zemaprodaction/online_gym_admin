/* tslint:disable:use-input-property-decorator */
import { Directive } from '@angular/core';
import { LayoutGapDirective } from '@angular/flex-layout';

const selector = `[fxLayoutGap], [fxLayoutGap.mobile],
[fxLayoutGap.sTablet], [fxLayoutGap.lTablet], [fxLayoutGap.lTabletLandscape], [fxLayoutGap.web]
`;

const inputs = [
  'fxLayoutGap',
  'fxLayoutGap.mobile',
  'fxLayoutGap.sTablet',
  'fxLayoutGap.lTablet',
  'fxLayoutGap.lTabletLandscape',
  'fxLayoutGap.web',
  'fxLayoutGap.hugeWeb'
];

@Directive({selector, inputs})
export class CustomLayoutGapDirective extends LayoutGapDirective {
  protected inputs = inputs;
}
