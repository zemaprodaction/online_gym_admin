/* tslint:disable:use-input-property-decorator */
import { Directive } from '@angular/core';
import { FlexDirective } from '@angular/flex-layout';

const selector = `[fxFlex], [fxFlex.mobile],
                  [fxFlex.sTablet], [fxFlex.lTablet], [fxFlex.lTabletLandscape],
                  [fxFlex.web], [fxFlex.lt-lTablet]
                  `;

const inputs = [
  'fxFlex',
  'fxFlex.mobile',
  'fxFlex.sTablet',
  'fxFlex.lt-lTablet',
  'fxFlex.lTablet',
  'fxFlex.lTabletLandscape',
  'fxFlex.web',
  'fxFlex.hugeWeb'
];

@Directive({selector, inputs})
export class CustomFlexDirective extends FlexDirective {
  protected inputs = inputs;
}
