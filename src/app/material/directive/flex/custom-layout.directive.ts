/* tslint:disable:use-input-property-decorator */
import {Directive} from '@angular/core';
import {LayoutDirective} from '@angular/flex-layout';

const selector = `[fxLayout], [fxLayout.mobile],
                  [fxLayout.sTablet], [fxLayout.lTablet], [fxLayout.lTabletLandscape],
                  [fxLayout.web], [fxLayout.lt-lTablet]
`;

const inputs = [
  'fxLayout',
  'fxLayout.mobile',
  'fxLayout.sTablet',
  'fxLayout.lTablet',
  'fxLayout.lTabletLandscape',
  'fxLayout.web',
  'fxLayout.lt-lTablet'

];

@Directive({selector, inputs})
export class CustomLayoutDirective extends LayoutDirective {
  protected inputs = inputs;
}
