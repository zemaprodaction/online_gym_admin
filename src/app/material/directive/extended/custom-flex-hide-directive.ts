/* tslint:disable:use-input-property-decorator */
import {Directive} from '@angular/core';
import {ShowHideDirective} from '@angular/flex-layout';

const selector = `[fxHide], [fxHide.mobile],
                  [fxHide.sTablet], [fxHide.lTablet], [fxHide.lTabletLandscape],
                  [fxHide.web]
                  `;

const inputs = [
  'fxHide',
  'fxHide.mobile',
  'fxHide.sTablet',
  'fxHide.lTablet',
  'fxHide.lTabletLandscape',
  'fxHide.web'
];

@Directive({selector, inputs})
export class CustomFlexHideDirective extends ShowHideDirective {
  protected inputs = inputs;
}
