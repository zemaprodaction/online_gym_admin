import {CustomFlexDirective} from './flex/custom-flex-directive';
import {CustomLayoutDirective} from './flex/custom-layout.directive';
import {CustomLayoutGapDirective} from './flex/custom-layet-gap.directive';
import {CustomLayoutAlignDirective} from './flex/custom-layout-align.directive';
import {CustomFlexHideDirective} from './extended/custom-flex-hide-directive';


export const directives = [CustomFlexDirective, CustomLayoutDirective,
  CustomLayoutGapDirective, CustomLayoutAlignDirective, CustomFlexHideDirective];

export * from './flex/custom-flex-directive';
export * from './flex/custom-layout.directive';
export * from './flex/custom-layet-gap.directive';
export * from './flex/custom-layout-align.directive';
export * from './extended/custom-flex-hide-directive';
