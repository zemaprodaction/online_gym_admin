export enum CustomBreakpoints {
  MOBILE = '(max-width: 599px)',
  S_TABLET = '(min-width: 600px) and (max-width: 824px)',
  L_TABLET = '(min-width: 825px) and (max-width: 1082px)',
  L_TABLET_LT = '(max-width: 1082px)',
  L_TABLET_LANDSCAPE = '(min-width: 1083px) and (max-width: 1439px)',
  WEB = '(min-width: 1440px) and (max-width: 1699px)',
  HUGE_WEB = '(min-width: 1700px)'
}

export const CUSTOM_BREAKPOINTS = [
  {alias: 'mobile', mediaQuery: CustomBreakpoints.MOBILE},
  {alias: 'sTablet', mediaQuery: CustomBreakpoints.S_TABLET},
  {alias: 'lTablet', mediaQuery: CustomBreakpoints.L_TABLET},
  {alias: 'lt-lTablet', mediaQuery: CustomBreakpoints.L_TABLET_LT},
  {alias: 'lTabletLandscape', mediaQuery: CustomBreakpoints.L_TABLET_LANDSCAPE},
  {alias: 'web', mediaQuery: CustomBreakpoints.WEB},
  {alias: 'hugeWeb', mediaQuery: CustomBreakpoints.HUGE_WEB}
];
