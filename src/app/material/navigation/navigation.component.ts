import {ChangeDetectionStrategy, Component, Input, OnInit, ViewChild} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {filter, map, withLatestFrom} from 'rxjs/operators';
import {CustomBreakpoints} from '../common/custom-breakpoints';
import {NavigationEnd, Router} from '@angular/router';
import {MatSidenav} from '@angular/material/sidenav';
import {User} from '../../authorization/model/user';


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavigationComponent implements OnInit {

  @Input()
  user: User;
  constructor(private breakpointObserver: BreakpointObserver, private router: Router) {
    router.events.pipe(
      withLatestFrom(this.isHandset$),
      filter(([a, b]) => b && a instanceof NavigationEnd)
    ).subscribe(() => this.sideNav.close());
  }

  @ViewChild('sidenav')
  sideNav: MatSidenav;

  sizeVisible: string[] = [CustomBreakpoints.WEB, CustomBreakpoints.HUGE_WEB];
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(this.sizeVisible)
  .pipe(
    map(result => !result.matches)
  );
  width$: Observable<string>;

  loading = true;

  ngOnInit(): void {
    this.width$ = this.isHandset$.pipe(
      filter(result => result),
      map(() => '100%')
    );
  }
}
