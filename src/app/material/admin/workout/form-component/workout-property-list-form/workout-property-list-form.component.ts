import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../../../form-components/model/abstract-form-component';
import {FormBuilder} from '@angular/forms';
import {WorkoutPropertyTypeKey} from '../../../../../share/type/workout/workout-property';
import {MetadataService} from '../../../../../share/services/metadata.service';
import {CommonTrainingInfo} from '../../../../../trainings/models/сommonTrainingInfo';

@Component({
  selector: 'app-workout-property-list-form',
  templateUrl: './workout-property-list-form.component.html',
  styleUrls: ['./workout-property-list-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutPropertyListFormComponent extends AbstractFormComponent implements OnInit {

  @Input()
  commonTrainingInfo: CommonTrainingInfo;

  keys: { key: string, type?: 'array' | 'object' }[];
  values: any[];
  displayWith: Function = (val) => val.id;

  constructor(private fb: FormBuilder, private metadataService: MetadataService,
              private changeDetectionRef: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    this.metadataService.getValueOfCharacteristicByEntityName('workout')
      .subscribe((val) => {
        this.setUpEquipmentWorkoutropertyToParent(val);
        this.changeDetectionRef.detectChanges();
      });
  }

  private setUpEquipmentWorkoutropertyToParent(val: any) {
    this.keys = Object.keys(val).map(k => ({
      key: k,
    }));
    this.values = val;
    this.initWorkoutPropertyArray();
  }

  private initWorkoutPropertyArray() {
    const form = this.fb.array([]);
    this.keys.forEach(wpKey => {
      const key = WorkoutPropertyTypeKey[wpKey.key];
      const valueByKey = this.commonTrainingInfo ? this.commonTrainingInfo[key] : '';
      if (Array.isArray(valueByKey)) {
        wpKey.type = 'array';
        this.parent.setControl(key, this.fb.array(valueByKey));
      } else {
        wpKey.type = 'object';
        this.parent.setControl(key, this.fb.control(valueByKey));
      }
    });
  }

  public getGroupNameByKey(key: string) {
    return WorkoutPropertyTypeKey[key];
  }

  getValueForWorkoutProperty(key: string) {
    return this.commonTrainingInfo[WorkoutPropertyTypeKey[key]];
  }

  compareWorkoutProperty(o1: any, o2: any) {
    return o1.id === o2.id && o1.id === o2.id;
  }
}
