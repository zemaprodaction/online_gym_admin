import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Workout} from '../../../../../trainings/models/workout/workout';
import {FormFieldModel} from '../../../form-components/model/form-field-model';

@Component({
  selector: 'app-add',
  templateUrl: './save-form.component.html',
  styleUrls: ['./save-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveUpdateWorkoutFormComponent implements OnInit {

  form: FormGroup;
  common: FormGroup;
  id: FormFieldModel = {name: 'Id', formControlName: 'id', placeholder: 'Enter Id'};
  duration: FormFieldModel = {name: 'Duration', formControlName: 'duration', placeholder: 'Enter Duration'};

  workout: Workout;

  constructor(public dialogRef: MatDialogRef<SaveUpdateWorkoutFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { row: Workout, header: string },
              private fb: FormBuilder) {

  }

  ngOnInit() {
    this.workout = this.data.row ? this.data.row : new Workout();
    this.form = this.initForm(this.workout);
  }

  getFormValue() {
    return this.form.getRawValue();
  }

  private initForm(workout: Workout) {
    return this.fb.group({
      id: [workout.id ? workout.id : ''],
      listOfPhases: this.fb.array(workout.listOfPhases || []),
      duration: workout ? workout.duration : 0
    });
  }


}
