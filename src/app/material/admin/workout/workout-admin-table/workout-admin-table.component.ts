import {Component} from '@angular/core';
import {CrudAbstractAdminPanelComponent} from '../../crud-abstract-admin-panel/crud-abstract-admin-panel.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import {SaveUpdateWorkoutFormComponent} from '../dialogs/save/save-form.component';
import {Workout} from '../../../../trainings/models/workout/workout';
import {CommonTrainingInfo} from '../../../../trainings/models/сommonTrainingInfo';
import {Observable} from 'rxjs';
import * as fromStore from '../../../../trainings/store';
import {Store} from '@ngrx/store';
import {DialogConfigurationService} from '../../servises/servises-module/dialog-configuration.service';

@Component({
  selector: 'app-workout-admin-table',
  templateUrl: './workout-admin-table.component.html',
  styleUrls: ['./workout-admin-table.component.css']
})
export class WorkoutAdminTableComponent extends CrudAbstractAdminPanelComponent {
  displayColumns = ['id', 'duration'];

  constructor(protected dialog: MatDialog, protected store: Store<fromStore.TrainingState>,
              private config: DialogConfigurationService) {
    super(dialog, store);
  }

  get saveForm(): any {
    return SaveUpdateWorkoutFormComponent;
  }

  protected dialogCreateConfig(): MatDialogConfig {
    const workout = new Workout();
    workout.common = new CommonTrainingInfo();

    return {data: {header: 'New Workout', workout}, ...this.config.getDialogPopUpConfig()};
  }

  protected dialogUpdateConfig(row: any): Observable<{ data: {} }> {
    return this.getInfoFromStore(row, fromStore.geWorkoutByID,
      {...this.config.getDialogPopUpConfig(), header: row.name});
}


}
