import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {TypeFormControl} from '../model/type-form-control';
import {AbstractFormComponent} from '../model/abstract-form-component';

@Component({
  selector: 'app-measure-count-fields',
  templateUrl: './measure-count-fields.component.html',
  styleUrls: ['./measure-count-fields.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeasureCountFieldsComponent extends AbstractFormComponent implements OnInit {

  typeFormControl = TypeFormControl;

  @Input()
  formGroupName: string;

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
