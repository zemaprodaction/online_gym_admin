import {Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../model/abstract-form-component';
import {ImageBundle} from '../../../../share/type/exercise/exercise';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FormFieldModel} from '../model/form-field-model';

@Component({
  selector: 'app-image-bundle-form',
  templateUrl: './image-bundle-form.component.html',
  styleUrls: ['./image-bundle-form.component.scss']
})
export class ImageBundleFormComponent extends AbstractFormComponent implements OnInit {

  @Input()
  imageBundle: ImageBundle;

  imageBundleControl: FormGroup;

  formImageBundleNames: FormFieldModel[] = [
    FormFieldModel.getNewInstance({
      name: 'WebImageLink',
      formControlName: 'webImageLink',
      placeholder: 'Enter webImgLink'
    }),
    FormFieldModel.getNewInstance({
      name: 'MobileImageLink',
      formControlName: 'mobileImageLink',
      placeholder: 'Enter mobImgLink'
    }),
  ];

  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.imageBundle = this.imageBundle ? this.imageBundle : new ImageBundle();
    this.imageBundleControl = this.createImageBundleControl(this.imageBundle);
    this.parent.setControl(this.controlName, this.imageBundleControl);
  }

  private createImageBundleControl(imageBundle: ImageBundle) {
    const control = this.fb.group({
      mobileImageLink: this.fb.control(imageBundle.mobileImageLink ? imageBundle.mobileImageLink : ''),
      webImageLink: this.fb.control(imageBundle.webImageLink ? imageBundle.webImageLink : '')
    });
    return control;
  }
}
