import {Input, Directive} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

@Directive()
export class AbstractFormComponent {
  @Input()
  parent: FormGroup;
  @Input()
  controlName: string;
  @Input()
  label = '';

}
