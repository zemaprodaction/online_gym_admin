import {TypeFormControl} from './type-form-control';

export class FormFieldModel {
  name: string;
  formControlName: string;
  placeholder: string;
  value?: string;
  type?: TypeFormControl;
  disable?: boolean;

  constructor(name: string, formControlName: string, placeholder: string,
              type: TypeFormControl = TypeFormControl.TEXT_INPUT,
              value?: string) {
    this.name = name;
    this.formControlName = formControlName;
    this.placeholder = placeholder;
    this.value = value;
    this.type = type;
  }

  static getNewInstance(formFieldModel: FormFieldModel) {
    return new FormFieldModel(formFieldModel.name, formFieldModel.formControlName,
      formFieldModel.placeholder, formFieldModel.type, formFieldModel.value);
  }
}
