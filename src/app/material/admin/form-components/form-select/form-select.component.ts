import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../model/abstract-form-component';

@Component({
  selector: 'app-form-select',
  templateUrl: './form-select.component.html',
  styleUrls: ['./form-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormSelectComponent extends AbstractFormComponent implements OnInit {

  constructor() {
    super();
  }

  @Input()
  selectControlName: string;
  @Input()
  items: any[];
  @Input()
  selectLabel = 'Select Items';
  @Input()
  pathSelectProperty = '';
  @Input()
  viewWith: Function = (obj: any) => obj
  ngOnInit() {
  }

  compareWith(o1: any, o2: any): boolean {
    return o1.id === o2.id;
  }

}
