import {ChangeDetectionStrategy, Component, ContentChild, Input, OnInit, TemplateRef} from '@angular/core';
import {AbstractFormComponent} from '../model/abstract-form-component';

@Component({
  selector: 'app-stepper-field',
  templateUrl: './stepper-field.component.html',
  styleUrls: ['./stepper-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StepperFieldComponent extends AbstractFormComponent implements OnInit {

  @Input() data: any;
  @Input() steps: Function;
  @Input() contextElement: any;
  @ContentChild('lineContent') lineContentTmpl: TemplateRef<any>;

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
