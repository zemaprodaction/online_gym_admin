import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../model/abstract-form-component';
import {FormFieldModel} from '../model/form-field-model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CommonTrainingInfo} from '../../../../trainings/models/сommonTrainingInfo';

@Component({
  selector: 'app-common-info',
  templateUrl: './common-info.component.html',
  styleUrls: ['./common-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommonInfoComponent extends AbstractFormComponent implements OnInit {

  id: FormFieldModel = {name: 'Id', formControlName: 'id', placeholder: 'Enter Id'};
  commonFormGroup: FormGroup;

  @Input()
  common: CommonTrainingInfo;

  constructor(private fb: FormBuilder) {
    super();

  }

  ngOnInit() {
    this.common = this.common ? this.common : new CommonTrainingInfo();
    this.commonFormGroup = this.initCommonInfo(this.common);
    this.parent.addControl('common', this.commonFormGroup);
  }

  private initCommonInfo(commonTrainingInfo: CommonTrainingInfo) {
    return this.fb.group({
      name: [commonTrainingInfo ? commonTrainingInfo.name : ''],
      description: [commonTrainingInfo ? commonTrainingInfo.description : ''],
      imageUrl: [commonTrainingInfo ? commonTrainingInfo.imageBundle : ''],
    });
  }

}
