import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../model/abstract-form-component';
import {FormFieldModel} from '../model/form-field-model';
import {TypeFormControl} from '../model/type-form-control';

@Component({
  selector: 'app-simple-field-list',
  templateUrl: './simple-field-list.component.html',
  styleUrls: ['./simple-field-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpleFieldListComponent extends AbstractFormComponent implements OnInit {

  @Input()
  input: FormFieldModel[];

  typeFormControl = TypeFormControl;

  @Input()
  formGroupName: string;

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
