import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormArray} from '@angular/forms';
import {AbstractFormComponent} from '../model/abstract-form-component';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StockComponent extends AbstractFormComponent implements OnInit {

  constructor() {
    super();
  }

  @Input()
  stock: FormArray;
  @Output()
  remove = new EventEmitter<any>();
  @Input()
  showValue: Function = (item) => item

  ngOnInit() {
  }

  getValue(item): string {
    return this.showValue ? this.showValue(item.value) : item.value;
  }

  onRemove(index) {
    this.remove.emit(index);
  }
}
