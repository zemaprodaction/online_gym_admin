import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../model/abstract-form-component';
import {FormFieldModel} from '../model/form-field-model';

@Component({
  selector: 'app-simple-number-field',
  templateUrl: './simple-number-field.component.html',
  styleUrls: ['./simple-number-field.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpleNumberFieldComponent extends AbstractFormComponent implements OnInit {

  constructor() {
    super();
  }

  @Input()
  input: FormFieldModel;
  @Input()
  value: string;
  @Input()
  max = 1000;
  @Input()
  min = 1;

  ngOnInit() {
  }

}
