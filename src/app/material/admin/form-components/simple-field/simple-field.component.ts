import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../model/abstract-form-component';
import {FormFieldModel} from '../model/form-field-model';

@Component({
  selector: 'app-simple-field',
  templateUrl: './simple-field.component.html',
  styleUrls: ['./simple-field.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpleFieldComponent extends AbstractFormComponent implements OnInit {

  constructor() {
    super();
  }

  @Input()
  input: FormFieldModel;
  @Input()
  value: string;

  ngOnInit() {
  }

}
