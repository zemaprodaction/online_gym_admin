import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FormFieldModel} from '../model/form-field-model';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {LocalizationService} from '../../../../share/services/localization.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-dialog-localize-field',
  templateUrl: './dialog-localize-field.component.html',
  styleUrls: ['./dialog-localize-field.component.scss']
})
export class DialogLocalizeFieldComponent implements OnInit {

  form: FormGroup = new FormGroup({});
  formNames: FormFieldModel[];
  isLoading = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: { key: string },
              private localizationService: LocalizationService,
              private fb: FormBuilder) {
    this.formNames = [
      FormFieldModel.getNewInstance({name: 'EN', formControlName: 'en', placeholder: 'Enter eng value',
        disable: true}),
      FormFieldModel.getNewInstance({
        name: 'RU', formControlName: 'ru',
        placeholder: 'Введите русское значение'
      }),
    ];

  }

  ngOnInit(): void {
    this.localizationService.getRuString(this.data.key)
      .pipe(take(1))
      .subscribe(
      resp => {
        this.form = this.fb.group({
          en: this.data.key,
          ru: resp
        });
        this.isLoading = true;
      }
    );
  }

  onSubmit() {
    const formValue = this.form.value;
    const request = {
      [formValue.en]: formValue
    };
    this.localizationService.updateLocalization(request)
      .pipe(take(1))
      .subscribe();
  }

}
