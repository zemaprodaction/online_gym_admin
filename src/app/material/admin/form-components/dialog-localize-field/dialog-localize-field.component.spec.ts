import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogLocalizeFieldComponent } from './dialog-localize-field.component';

describe('DialogLocalizeFieldComponent', () => {
  let component: DialogLocalizeFieldComponent;
  let fixture: ComponentFixture<DialogLocalizeFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogLocalizeFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogLocalizeFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
