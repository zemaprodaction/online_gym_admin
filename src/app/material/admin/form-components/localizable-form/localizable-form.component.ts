import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../model/abstract-form-component';
import {FormArray, FormBuilder} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {DialogLocalizeFieldComponent} from '../dialog-localize-field/dialog-localize-field.component';

@Component({
  selector: 'app-localizable-form',
  templateUrl: './localizable-form.component.html',
  styleUrls: ['./localizable-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LocalizableFormComponent extends AbstractFormComponent implements OnInit {

  @Input()
  value = '';

  localizableFieldControl: FormArray;

  languages = ['en'];

  constructor(protected dialog: MatDialog,
              private fb: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    if (!this.parent) {
      this.controlName = 'name';
      this.parent = this.fb.group({
        name: this.fb.control(this.value)
      });
    } else {
      this.parent.setControl(this.controlName, this.fb.control(this.value));
    }
  }

  localizeField() {
    const dialogRef = this.dialog.open(DialogLocalizeFieldComponent, {
      data: {key: this.value}, width: '500px'
    });
  }
}
