import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {AbstractFormComponent} from '../model/abstract-form-component';
import {AbstractControl, FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {Exercise} from '../../../../share/type/exercise/exercise';

@Component({
  selector: 'app-autocomplete-multiply-form',
  templateUrl: './autocomplete-multiply-form.component.html',
  styleUrls: ['./autocomplete-multiply-form.component.scss'],
})
export class AutocompleteMultiplyFormComponent extends AbstractFormComponent implements OnInit {

  constructor(private fb: FormBuilder, private changeDetection: ChangeDetectorRef) {
    super();
    this.selectForm = this.fb.group({
      selectItem: this.fb.control('')
    });
  }

  selectForm: FormGroup;
  stockForm: FormArray;
  filteredItem: Observable<any[]>;

  @Input()
  allOptions: any[];

  @Input()
  chosenOptions: any[];
  chosenOptions2: any[];
  @Input()
  label: string;

  @Input()
  isPrimitiveArray = true;

  @Input()
  addedOnlyId = false;

  @Input()
  choosePropertyForValue = null;

  @Output()
  entityAdded: EventEmitter<any> = new EventEmitter();

  @Input()
  displayWith: Function = (val) => val;

  ngOnInit(): void {
    this.filteredItem = this.selectForm.get('selectItem').valueChanges
      .pipe(
        startWith(''),
        map(item => item ? this._filterItems(item) : this.allOptions.slice())
      );
    this.initStockForm(this.chosenOptions);
    this.parent.setControl(this.controlName, this.stockForm);
    this.chosenOptions2 = [...this.chosenOptions];
  }

  getShowImage(item: any): string {
    return item['imageUrl'];
  }

  getShowValue(item: any): string {
    return item['name'] ? item['name'] : item;
  }

  getShowDescription(item: any): string {
    return item['description'];
  }

  onAdd() {
    let newControl: AbstractControl;
    let value: any;

    if (this.addedOnlyId) {
      value = this.getSelectItem().value['id'];
    } else {
      value = this.getSelectItem().value;
    }
    newControl = this.fb.control(value);
    if (newControl.value) {
      this.stockForm.push(newControl);
      this.chosenOptions2.push(value);
    }
    this.entityAdded.emit(value);
    this.changeDetection.detectChanges();
  }

  onRemove(i: any) {
    this.chosenOptions2.splice(i, 1);
    this.stockForm.removeAt(i);
    this.changeDetection.detectChanges();
  }

  private _filterItems(value: string): string[] {
    const filterValue = this.displayWith(value) ? this.displayWith(value).toLowerCase() : value.toLowerCase();
    return this.allOptions.filter(item => {
      return this.isPrimitiveArray ?
        item.toLowerCase().indexOf(filterValue) === 0 : item.name[0].toLowerCase().indexOf(filterValue) === 0;
    });
  }

  private getSelectItem() {
    return this.selectForm.get('selectItem');
  }


  private initStockForm(chosenOptions: any[]) {
    this.stockForm = this.fb.array([]);
    if (chosenOptions) {
      chosenOptions.forEach(optipn => {
        this.stockForm.push(this.fb.control(this.choosePropertyForValue && optipn ? optipn[this.choosePropertyForValue] : optipn));
      });
    } else {
      this.stockForm = this.fb.array([]);
    }
  }

  drop(event: CdkDragDrop<Exercise[]>) {
    console.log(this.chosenOptions);
    console.log(event.previousIndex);
    console.log(event.currentIndex);
    moveItemInArray(this.chosenOptions2, event.previousIndex, event.currentIndex);
    moveItemInArray(this.stockForm.controls, event.previousIndex, event.currentIndex);
    console.log(this.chosenOptions);
  }

}

export function moveItemInFormArray(
  formArray: FormArray,
  fromIndex: number,
  toIndex: number
): void {
  const dir = toIndex > fromIndex ? 1 : -1;

  const item = formArray.at(fromIndex);
  for (let i = fromIndex; i * dir < toIndex * dir; i = i + dir) {
    const current = formArray.at(i + dir);
    formArray.setControl(i, current);
  }
  formArray.setControl(toIndex, item);
}
