import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormControl} from '@angular/forms';
import {AbstractFormComponent} from '../model/abstract-form-component';
import {Store} from '@ngrx/store';
import * as fromStore from '../../../../trainings/store';
import {Observable, of} from 'rxjs';
import {Exercise} from '../../../../share/type/exercise/exercise';
import {WorkoutPhase} from '../../../../trainings/models/workout/workout-phase';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StepperComponent extends AbstractFormComponent implements OnInit {

  @Input()
  phaseList: WorkoutPhase[];

  listOfPhaseFormArray: FormArray;

  constructor(private _formBuilder: FormBuilder, private store: Store<fromStore.TrainingState>) {
    super();
  }

  phaseName: FormControl;
  $exercisesList: Observable<Exercise[]>;

  ngOnInit() {
    this.phaseName = this._formBuilder.control('');
    this.parent.setControl('listOfPhases', this.getListOfPhaseFormArray());
    this.$exercisesList = this.store.select(fromStore.getAllExercises);

  }

  private getListOfPhaseFormArray() {
    this.listOfPhaseFormArray = this._formBuilder.array([]);
    if (this.phaseList) {
      this.phaseList.forEach(value => {
          this.listOfPhaseFormArray.push(this._formBuilder.group({
            name: [value.name],
            listOfExercises: this._formBuilder.array(value.listOfExercises),
            isOptional: [value.isOptional]
          }));
        }
      );
    }
    return this.listOfPhaseFormArray;
  }

  createPhase() {
    return this._formBuilder.group({
      name: this.phaseName.value,
      listOfExercises: this._formBuilder.array([]),
      isOptional: false
    });
  }

  addPhase() {
    this.listOfPhaseFormArray.push(this.createPhase());
  }

  displayExercise(exrcise: Exercise) {
    if (exrcise && exrcise.name) {
      return exrcise.name;
    } else {
      return exrcise;
    }
  }

  removePhase(i: number) {
    this.listOfPhaseFormArray.removeAt(i);
  }

  clone() {
    const lastPhase = this.listOfPhaseFormArray.value[this.listOfPhaseFormArray.value.length - 1];
    this.listOfPhaseFormArray.push(this._formBuilder.group(
      {
        name: lastPhase.name,
        listOfExercises: this._formBuilder.array([...lastPhase.listOfExercises]),
        isOptional: lastPhase.isOptional
      }
    ));
  }

  getExerciseForPhase(valueElement: string[]): Observable<Exercise[]> {
    if (valueElement) {
      return this.store.select(fromStore.getSelectExerciseByListID, {ids: valueElement});
    }
    return of([]);
  }
}
