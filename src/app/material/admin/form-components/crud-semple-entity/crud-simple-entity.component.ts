import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FormFieldModel} from '../model/form-field-model';
import {MetadataService} from '../../../../share/services/metadata.service';
import {Observable} from 'rxjs';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-crud-simple-entity',
  templateUrl: './crud-simple-entity.component.html',
  styleUrls: ['./crud-simple-entity.component.scss']
})
export class CrudSimpleEntityComponent implements OnInit {

  @Input()
  entityName: string;

  form: FormGroup;
  entityValues$: Observable<any[]>;

  entityValue: FormFieldModel = FormFieldModel.getNewInstance({
    name: 'Name',
    formControlName: 'name',
    placeholder: 'Enter name'
  });

  constructor(private fb: FormBuilder,
              private changeDetectorRefs: ChangeDetectorRef,
              private metadataService: MetadataService) {
    this.form = fb.group({
      name: this.fb.control('')
    });
  }

  ngOnInit(): void {
    this.entityValues$ = this.metadataService.getEntities(this.entityName);
  }

  onSubmit() {
    const val = {
      name: this.form.value.name
    };
    this.metadataService.saveEntity(this.entityName, val).pipe(
      take(1)
    ).subscribe(() => {
      this.entityValues$ = this.metadataService.getEntities(this.entityName);
      this.changeDetectorRefs.detectChanges();
    });
  }

  remove($event: string) {
    this.metadataService.removeEntity(this.entityName, $event).pipe(
      take(1)
    ).subscribe(() => {
      this.changeDetectorRefs.detectChanges();
    });
  }
}
