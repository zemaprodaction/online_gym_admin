import {Component, Input, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-list-entity',
  templateUrl: './list-entity.component.html',
  styleUrls: ['./list-entity.component.scss']
})
export class ListEntityComponent implements OnInit {

  @Input()
  entityList: any[];

  @Output()
  removeEntityEvent = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit(): void {
  }

  removeEntity(id) {
    this.entityList = this.entityList.filter((entity) => entity.id !== id);
    this.removeEntityEvent.emit(id);
  }
}
