import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SaveFormComponent} from './exercise/dialogs/save/save-form.component';
import {SaveUpdateWorkoutFormComponent} from './workout/dialogs/save/save-form.component';
import * as fromAdmin from './index';
import {DynamicTableComponent} from './dynamic-table/dynamic-table.component';
import {SimpleFieldListComponent} from './form-components/simple-field-list/simple-field-list.component';
import {SimpleFieldComponent} from './form-components/simple-field/simple-field.component';
import {SimpleNumberFieldComponent} from './form-components/simple-number-field/simple-number-field.component';
import {StepperComponent} from './form-components/stepper/stepper.component';
import {StockComponent} from './form-components/stock/stock.component';
import {MaterialModule} from '../material.module';
import {FormSelectComponent} from './form-components/form-select/form-select.component';
import {ServicesModule} from './servises/servises-module/services.module';
import {SaveUpdateWorkoutProgramFormComponent} from './workout-program/dialogs/save/save-form.component';
import {StepperFieldComponent} from './form-components/stepper-field/stepper-field.component';
import { MatCardModule } from '@angular/material/card';
import { MeasureCountFieldsComponent } from './form-components/measure-count-fields/measure-count-fields.component';
import { CommonInfoComponent } from './form-components/common-info/common-info.component';
import { ListOfWeeksComponent } from './workout-program/form-component/list-of-weeks/list-of-weeks.component';
import { ListOfDaysComponent } from './workout-program/form-component/list-of-days/list-of-days.component';
import { ListOfSchedulerWorkoutComponent } from './workout-program/form-component/list-of-scheduler-workout/list-of-scheduler-workout.component';
import { ListOfWorkoutPhasesComponent } from './workout-program/form-component/list-of-workout-phases/list-of-workout-phases.component';
import { ListOfSetsComponent } from './workout-program/form-component/list-of-sets/list-of-sets.component';
import {TrainingsModule} from '../../trainings/trainings.module';
import {ShareModule} from '../../share/share.module';
import { ImageBundleFormComponent } from './form-components/image-bundle-form/image-bundle-form.component';
import {AutocompleteMultiplyFormComponent} from './form-components/autocomplete-multiply-form/autocomplete-multiply-form.component';
import { LocalizableFormComponent } from './form-components/localizable-form/localizable-form.component';
import { AdditionalInfoFormComponent } from './exercise/form-component/additional-info-form/additional-info-form.component';
import { EquipmentFormComponent } from './exercise/form-component/equipment-form/equipment-form.component';
import { WorkoutPropertyListFormComponent } from './workout/form-component/workout-property-list-form/workout-property-list-form.component';
import { DialogLocalizeFieldComponent } from './form-components/dialog-localize-field/dialog-localize-field.component';
import { UserAdminComponent } from './user-admin/user-admin.component';
import {UserDialogComponent} from './user-admin/dialogs/save/save-form.component';
import { EntityCharacteristicComponent } from './workout-program/form-component/entity-characteristic/entity-characteristic.component';
import { CharacteristicPropertyItemComponent } from './workout-program/form-component/charateristic-property-item/characteristic-property-item.component';
import { CrudSimpleEntityComponent } from './form-components/crud-semple-entity/crud-simple-entity.component';
import { ListEntityComponent } from './form-components/list-entity/list-entity.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { VideoFormComponent } from './exercise/form-component/video-form/video-form.component';
import {DragDropModule} from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    ...fromAdmin.exerciseAdminTableComponents,
    ...fromAdmin.workoutProgramTableAdminComponents,
    ...fromAdmin.exerciseWorkoutAdmin,
    SaveFormComponent,
    SaveUpdateWorkoutFormComponent,
    SaveUpdateWorkoutProgramFormComponent,
    DynamicTableComponent,
    SimpleFieldListComponent,
    SimpleNumberFieldComponent,
    SimpleFieldComponent,
    StepperComponent,
    StockComponent,
    FormSelectComponent,
    StepperFieldComponent,
    MeasureCountFieldsComponent,
    CommonInfoComponent,
    ListOfWeeksComponent,
    ListOfDaysComponent,
    ListOfSchedulerWorkoutComponent,
    ListOfWorkoutPhasesComponent,
    ListOfSetsComponent,
    ImageBundleFormComponent,
    AutocompleteMultiplyFormComponent,
    LocalizableFormComponent,
    AdditionalInfoFormComponent,
    EquipmentFormComponent,
    WorkoutPropertyListFormComponent,
    DialogLocalizeFieldComponent,
    UserAdminComponent,
    UserDialogComponent,
    EntityCharacteristicComponent,
    CharacteristicPropertyItemComponent,
    CrudSimpleEntityComponent,
    ListEntityComponent,
    VideoFormComponent
  ],
    imports: [
      DragDropModule,
        CommonModule, MaterialModule, ServicesModule, MatCardModule, TrainingsModule, ShareModule, MatSlideToggleModule
    ],
  exports: [
    ...fromAdmin.exerciseAdminTableComponents,
    ...fromAdmin.workoutProgramTableAdminComponents,
    ...fromAdmin.exerciseWorkoutAdmin,
    UserAdminComponent,
    AutocompleteMultiplyFormComponent
  ],
  entryComponents: [],
})
export class AdminMaterialModule {
}
