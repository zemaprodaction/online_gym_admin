import { EventEmitter, Input, Output, Directive } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import * as fromStore from '../../../trainings/store';

@Directive()
export abstract class CrudAbstractAdminPanelComponent {

  @Input()
  entitiesList: any[];

  @Output()
  deleteItemOn: EventEmitter<any> = new EventEmitter();
  @Output()
  createItemOn: EventEmitter<any> = new EventEmitter();
  @Output()
  updateItemOn: EventEmitter<any> = new EventEmitter();

  protected constructor(protected dialog: MatDialog, protected store: Store<fromStore.TrainingState>) {
  }

  get saveForm(): any {
    return null;
  }

  protected abstract dialogCreateConfig(): MatDialogConfig;

  protected abstract dialogUpdateConfig(row: any): Observable<{ data: {} }>;


  deleteEntity(row: any) {
    this.deleteItemOn.emit(row);
  }

  createEntity() {
    const dialogRef = this.dialog.open(this.saveForm, this.dialogCreateConfig());
    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        return this.createItemOn.emit(data);
      }
    });
  }

  editEntity(row: any) {
    let dialogRef;
    this.dialogUpdateConfig(row).subscribe(
      data => {
        dialogRef = this.dialog.open(this.saveForm, data);
        dialogRef.afterClosed().subscribe((dialogData) => {
          if (dialogData) {
            return this.updateItemOn.emit(dialogData);
          }
        });
      }
    );
  }
  protected getInfoFromStore(row: any, select, config) {
    return this.store.select(select, {id: row.id}).pipe(
      take(1),
      map(tableRow => {
        return {data: {row: tableRow, header: config.header}, ...config};
      })
    );
  }
}
