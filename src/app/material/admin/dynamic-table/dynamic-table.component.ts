import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Exercise} from '../../../share/type/exercise/exercise';

@Component({
  selector: 'app-dynamic-table',
  templateUrl: './dynamic-table.component.html',
  styleUrls: ['./dynamic-table.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicTableComponent implements OnInit {

  @Input()
  entityList: any;
  @Input()
  displayedColumns: string[];

  autoBuildColumn: string[];

  constructor() {
  }

  @Output()
  deleteExerciseOn: EventEmitter<Exercise> = new EventEmitter();
  @Output()
  createExerciseOn: EventEmitter<Exercise> = new EventEmitter();
  @Output()
  updateExerciseOn: EventEmitter<Exercise> = new EventEmitter();

  ngOnInit() {
    this.displayedColumns = this.displayedColumns ? this.displayedColumns
      : Object.getOwnPropertyNames(this.entityList[0] || {});
    this.autoBuildColumn = [...this.displayedColumns, 'control'];
  }

  deleteExercise(row: any) {
    this.deleteExerciseOn.emit(row);
  }

  addNew() {
    return this.createExerciseOn.emit();
  }

  editExercise(row: any) {
    return this.updateExerciseOn.emit(row);
  }

}
