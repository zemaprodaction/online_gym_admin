import {Injectable} from '@angular/core';
import {ServicesModule} from "./services.module";

@Injectable({
  providedIn: ServicesModule
})
export class DialogConfigurationService {

  getDialogPopUpConfig(){
    return  {height: '700px', width:' 900px', header: ''};
  }

  getLargeDialogConfig(){
    return  {height: '90%', width:'90%', header: ''};
  }
  constructor() { }
}
