import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Exercise} from '../../../../../share/type/exercise/exercise';
import {FormFieldModel} from '../../../form-components/model/form-field-model';
import {ExerciseType} from '../../../../../share/type/exercise/exercise-type.enum';

@Component({
  selector: 'app-add',
  templateUrl: './save-form.component.html',
  styleUrls: ['./save-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveFormComponent implements OnInit {

  form: FormGroup = new FormGroup({});
  video: FormGroup = new FormGroup({});

  exerciseTypes = ExerciseType;
  exercise: Exercise;

  formNames: FormFieldModel[] = [
    FormFieldModel.getNewInstance({name: 'Id', formControlName: 'id', placeholder: 'Enter Id'}),
  ];


  constructor(public dialogRef: MatDialogRef<SaveFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { row: Exercise, header: string }, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.exercise = this.data.row ? this.data.row : new Exercise();
    this.form = this.fb.group({
      name: [this.exercise.name ? this.exercise.name : ''],
      id: [this.exercise.id ? this.exercise.id : ''],
      exerciseType: [this.exercise.exerciseType ? this.exercise.exerciseType : ''],
    });
  }

  onSubmit() {
  }
}
