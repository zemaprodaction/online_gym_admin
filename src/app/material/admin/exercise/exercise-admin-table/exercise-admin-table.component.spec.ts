import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseAdminTableComponent } from './exercise-admin-table.component';

describe('ExerciseAdminTableComponent', () => {
  let component: ExerciseAdminTableComponent;
  let fixture: ComponentFixture<ExerciseAdminTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExerciseAdminTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExerciseAdminTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
