import {ChangeDetectionStrategy, Component} from '@angular/core';
import {SaveFormComponent} from '../dialogs/save/save-form.component';
import {Exercise} from '../../../../share/type/exercise/exercise';
import {CrudAbstractAdminPanelComponent} from '../../crud-abstract-admin-panel/crud-abstract-admin-panel.component';
import {MatDialog} from '@angular/material/dialog';
import {Store} from '@ngrx/store';
import * as fromStore from '../../../../trainings/store';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-exercise-admin-table',
  templateUrl: './exercise-admin-table.component.html',
  styleUrls: ['./exercise-admin-table.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExerciseAdminTableComponent extends CrudAbstractAdminPanelComponent {

  displayedColumns = ['name', 'exerciseType'];

  constructor(protected dialog: MatDialog, protected store: Store<fromStore.TrainingState>) {
    super(dialog, store);
  }

  get saveForm(): any {
    return SaveFormComponent;
  }

  protected dialogCreateConfig(): { data: {} } {
    return {data: {header: 'header', row: new Exercise()}};
  }

  protected dialogUpdateConfig(row: any): Observable<{ data: {} }> {
    return this.getInfoFromStore(row, fromStore.getSelectExerciseByID, {header: row.name});
  }
}
