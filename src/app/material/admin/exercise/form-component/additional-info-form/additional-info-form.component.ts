import {Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../../../form-components/model/abstract-form-component';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {AdditionalInfo} from '../../../../../share/type/exercise/additional-info';
import {Observable} from 'rxjs';
import {MetadataService} from '../../../../../share/services/metadata.service';
import {Muscle} from '../../../../../share/type/muscle';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-additional-info-form',
  templateUrl: './additional-info-form.component.html',
  styleUrls: ['./additional-info-form.component.scss']
})
export class AdditionalInfoFormComponent extends AbstractFormComponent implements OnInit {

  @Input()
  additionalInfo: AdditionalInfo;

  additionalFormInfoControl: FormGroup;
  musclesAllOptions$: Observable<Muscle[]>;

  newMuscleForm: FormGroup;

  displayWith: Function = (val) => val.name;

  constructor(private fb: FormBuilder, private metadataService: MetadataService) {
    super();
    this.newMuscleForm = this.fb.group({
      name: this.fb.control('')
    });
    this.musclesAllOptions$ = this.metadataService.getMuscles();
  }

  ngOnInit(): void {
    this.parent.setControl('additionalInfo', this.getAdditionalInfoControl());
  }

  private getAdditionalInfoControl() {
    this.additionalFormInfoControl = this.fb.group({});
    return this.additionalFormInfoControl;
  }

  muscleAdded() {
    const muscle = this.newMuscleForm.value;
    this.metadataService.saveMuscle(muscle)
      .pipe(take(1))
      .subscribe(() => {
      });
  }
}
