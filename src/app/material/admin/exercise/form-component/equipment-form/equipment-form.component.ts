import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../../../form-components/model/abstract-form-component';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {Equipment} from '../../../../../share/type/equipment';
import {FormFieldModel} from '../../../form-components/model/form-field-model';
import {Observable} from 'rxjs';
import {EquipmentServiceApi} from '../../../../../share/services/equipment-service-api.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-equipment-form',
  templateUrl: './equipment-form.component.html',
  styleUrls: ['./equipment-form.component.scss']
})
export class EquipmentFormComponent extends AbstractFormComponent implements OnInit {

  @Input()
  equipment: Equipment[];
  @Input()
  $equipmentList: Observable<Equipment[]>;

  equipmentArrayControl: FormArray;

  newEquipmentForm: FormGroup;

  formNames: FormFieldModel[] = [
    FormFieldModel.getNewInstance({name: 'Id', formControlName: 'id', placeholder: 'Enter Id'}),
  ];

  constructor(private fb: FormBuilder,
              private changeDetection: ChangeDetectorRef,
              private equipmentService: EquipmentServiceApi) {
    super();
  }

  ngOnInit(): void {
    this.parent.setControl('equipment', this.getEquipmentArrayControl());
    this.$equipmentList = this.equipmentService.getAllEquipment();
    this.newEquipmentForm = this.fb.group({
      id: this.fb.control(''),
      name: this.fb.control('')
    });
  }

  private getEquipmentArrayControl() {
    this.equipmentArrayControl = this.fb.array([]);
    this.initEquipmentArrayControl(this.equipment);
    return this.equipmentArrayControl;
  }

  addEquipment() {
    const equipment = this.newEquipmentForm.value;
    this.equipmentService.addEquipment(equipment)
      .pipe(take(1))
      .subscribe(() => {
        this.changeDetection.detectChanges();
      });
  }


  displayEquipment(equipment: Equipment) {

    if (equipment.name) {
      return equipment.name;
    } else {
      return equipment;
    }
  }

  private initEquipmentArrayControl(equipment: Equipment[]) {
    if (equipment) {
      equipment.forEach(e => {
        this.equipmentArrayControl.push(this.fb.group({
          id: this.fb.control(e.id),
          name: [e.name],
        }));
      });
    }
  }
}
