import {Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../../../form-components/model/abstract-form-component';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FormFieldModel} from '../../../form-components/model/form-field-model';
import {Video} from '../../../../../share/type/exercise/video';

@Component({
  selector: 'app-video-form',
  templateUrl: './video-form.component.html',
  styleUrls: ['./video-form.component.scss']
})
export class VideoFormComponent extends AbstractFormComponent implements OnInit {

  @Input()
  video: Video;

  videoControl: FormGroup;

  formVideoNames: FormFieldModel[] = [
    FormFieldModel.getNewInstance({
      name: 'URl',
      formControlName: 'url',
      placeholder: 'Enter url'
    }),
  ];

  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.video = this.video ? this.video : {url: ''};
    this.videoControl = this.createVideoControl(this.video);
    this.parent.setControl(this.controlName, this.videoControl);
  }

  private createVideoControl(video: Video) {
    return this.fb.group({
      url: this.fb.control(video.url ? video.url : ''),
    });
  }

}
