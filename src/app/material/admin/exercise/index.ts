import {ExerciseAdminTableComponent} from './exercise-admin-table/exercise-admin-table.component';

export const exerciseAdminTableComponents = [ExerciseAdminTableComponent];

export * from './exercise-admin-table/exercise-admin-table.component';
