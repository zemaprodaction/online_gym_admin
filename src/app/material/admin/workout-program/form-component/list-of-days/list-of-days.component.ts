import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../../../form-components/model/abstract-form-component';
import {FormArray, FormBuilder} from '@angular/forms';
import {Observable} from 'rxjs';
import {Workout} from '../../../../../trainings/models/workout/workout';
import * as fromStore from '../../../../../trainings/store';
import {Store} from '@ngrx/store';
import {WorkoutDay} from '../../../../../trainings/models/workout-programs';
import {FormFieldModel} from '../../../form-components/model/form-field-model';
import {TypeFormControl} from '../../../form-components/model/type-form-control';

@Component({
  selector: 'app-list-of-days',
  templateUrl: './list-of-days.component.html',
  styleUrls: ['./list-of-days.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListOfDaysComponent extends AbstractFormComponent implements OnInit {

  @Input()
  listOfDays: WorkoutDay[];

  listOfDaysFormArray: FormArray;

  dayIndexFormModel = new FormFieldModel('day', 'dayIndex',
    'Enter Day', TypeFormControl.TEXT_INPUT);

  $workouts: Observable<Workout[]>;
  constructor(private fb: FormBuilder, private store: Store<fromStore.TrainingState>) {
    super();
  }

  ngOnInit() {
    this.listOfDays = this.listOfDays ? this.listOfDays : [{}];
    this.parent.setControl('listOfDays', this.initListOfDays(this.listOfDays));
    this.$workouts = this.store.select(fromStore.getAllWorkouts);

  }

  private initListOfDays(days: WorkoutDay[]) {
    this.listOfDaysFormArray = this.fb.array([]);
    if (days) {
      days.forEach(day => {
        this.listOfDaysFormArray.push(this.fb.group({
          dayIndex: this.fb.control(day.dayIndex)
        }));
      });
    }
    return this.listOfDaysFormArray;
  }

  addDay() {
    this.listOfDaysFormArray.push((this.fb.group({
      listOfScheduledWorkouts: this.fb.array([]),
      dayIndex: this.fb.control(1)
    })));
  }

  removeDay(index: number) {
    this.listOfDaysFormArray.removeAt(index);
  }

  getDay(dayIndex: number) {
    return this.listOfDays ? this.listOfDays[dayIndex] : null;
  }
}
