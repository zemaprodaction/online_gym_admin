import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../../../form-components/model/abstract-form-component';
import {FormArray, FormBuilder} from '@angular/forms';
import {WorkoutSet} from '../../../../../trainings/models/workout/workout-set';
import {FormFieldModel} from '../../../form-components/model/form-field-model';
import {MeasureType} from '../../../../../trainings/models/measureType';
import {Observable} from 'rxjs';
import {getAllMeasureType} from '../../../../../trainings/store/selectors/measure-type.selector';
import {Store} from '@ngrx/store';
import * as fromStore from '../../../../../trainings/store';
import {Exercise} from '../../../../../share/type/exercise/exercise';

@Component({
  selector: 'app-list-of-sets',
  templateUrl: './list-of-sets.component.html',
  styleUrls: ['./list-of-sets.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListOfSetsComponent extends AbstractFormComponent implements OnInit {

  @Input()
  listOfSet: WorkoutSet[];

  listOfWorkoutSetFormArray: FormArray;

  listOfMeasureTypes$: Observable<MeasureType[]>;

  expectedValue: FormFieldModel = FormFieldModel.getNewInstance({
    name: 'Expected Value',
    formControlName: 'expectedValue',
    placeholder: 'Enter expectedValue'
  });
  restTime: FormFieldModel = FormFieldModel.getNewInstance({
    name: 'Rest time',
    formControlName: 'restTime',
    placeholder: 'Enter rest time'
  });

  constructor(private fb: FormBuilder,
              private store: Store<fromStore.TrainingState>) {
    super();
  }

  viewWith: Function = (measureType: MeasureType) => {
    return measureType.id;
  };

  ngOnInit() {
    this.listOfMeasureTypes$ = this.store.select(getAllMeasureType);
    this.parent.setControl('listOfSets', this.initListOfSets());
  }

  private initListOfSets() {
    this.listOfWorkoutSetFormArray = this.fb.array([]);
    if (this.listOfSet) {
      this.listOfSet.forEach(set => {
        this.listOfWorkoutSetFormArray.push(this.fb.group({
          exerciseId: set.exerciseId,
          listOfMeasures: this.initListOfMeasures(set),
          restTime: set.restTime,
        }));
      });
    }
    return this.listOfWorkoutSetFormArray;
  }

  private initListOfMeasures(set: WorkoutSet) {
    const listOfMeasuresForm = this.fb.array([]);
    if (set.listOfMeasures) {
      set.listOfMeasures.forEach(measure => {
        listOfMeasuresForm.push(this.fb.group({
          expectedValue: measure.expectedValue,
          measureTypeId: measure.measureTypeId,
        }));
      });
    }
    return listOfMeasuresForm;
  }

  removeMeasure(set, index: number) {
    set.controls.listOfMeasures.removeAt(index);
    set.controls.listOfMeasures.controls = [...set.controls.listOfMeasures.controls];
  }

  addMeasure(set) {
    set.controls.listOfMeasures.push((this.fb.group({
      expectedValue: '',
      measureTypeId: ''
    })));
  }


  getExerciseName(setIndex: number): Observable<Exercise> {
    if (this.listOfSet[setIndex]) {
      if (this.listOfSet[setIndex].exerciseId === '') {
        if (this.listOfSet[setIndex].exerciseId) {
          return this.store.select(fromStore.getSelectExerciseByID, {id: this.listOfSet[setIndex].exerciseId});
        }
      }
      return this.store.select(fromStore.getSelectExerciseByID, {id: this.listOfSet[setIndex].exerciseId});
    }
  }
}
