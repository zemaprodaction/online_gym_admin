import {Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../../../form-components/model/abstract-form-component';
import {CharacteristicPropertyFilter} from '../../../../../trainings/models/characteristic-property-filter';
import {CharacteristicProperty} from '../../../../../trainings/models/characteristic-property';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-characteristic-property-item',
  templateUrl: './characteristic-property-item.component.html',
  styleUrls: ['./characteristic-property-item.component.scss']
})
export class CharacteristicPropertyItemComponent extends AbstractFormComponent implements OnInit {

  @Input()
  characteristicPropertyFilter: CharacteristicPropertyFilter;

  @Input()
  chooseValue: CharacteristicProperty;

  formGroup: FormGroup;
  isArray = false;

  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.initFormProperty();
  }

  compareValue(o1: any, o2: any) {
    return o1 === o2;
  }

  private initFormProperty() {
    this.formGroup = this.fb.group({
      name: this.characteristicPropertyFilter.name,
      id: this.characteristicPropertyFilter.id,
      nativeValue: this.getValueControl(),
    });

    this.parent.setControl(this.characteristicPropertyFilter.filterPath, this.formGroup);

  }

  private getValueControl() {
    if (Array.isArray(this.chooseValue?.nativeValue)) {
      return this.fb.array(this.chooseValue.nativeValue);
    } else {
      return this.chooseValue?.nativeValue || '';
    }
  }
}
