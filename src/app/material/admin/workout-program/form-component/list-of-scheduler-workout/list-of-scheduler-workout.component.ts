import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../../../form-components/model/abstract-form-component';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {WorkoutInstance} from '../../../../../trainings/models/workout/workout-instance';
import {Workout} from '../../../../../trainings/models/workout/workout';
import {Store} from '@ngrx/store';
import * as fromStore from '../../../../../trainings/store';
import {Observable} from 'rxjs';
import {WorkoutSet} from '../../../../../trainings/models/workout/workout-set';
import {WorkoutPhase} from '../../../../../trainings/models/workout/workout-phase';
import {WorkoutInstancePhase} from '../../../../../trainings/models/workout/workout-instance-phase';

@Component({
  selector: 'app-list-of-scheduler-workout',
  templateUrl: './list-of-scheduler-workout.component.html',
  styleUrls: ['./list-of-scheduler-workout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListOfSchedulerWorkoutComponent extends AbstractFormComponent implements OnInit {

  @Input()
  listWorkoutInstances: WorkoutInstance[];

  listOfScheduledWorkouts: FormArray;

  $workouts: Observable<Workout[]>;

  formSelectGroup: FormGroup;
  viewWith: Function = (workout: Workout) => {
    return workout.common.name;
  };

  constructor(private fb: FormBuilder, private store: Store<fromStore.TrainingState>) {
    super();
  }

  ngOnInit() {
    this.$workouts = this.store.select(fromStore.getAllWorkouts);
    const listOfScheduledWorkouts = this.listWorkoutInstances ? this.listWorkoutInstances : [];

    this.parent.setControl('listOfScheduledWorkouts', this.initListOfSchedulerWorkout(listOfScheduledWorkouts));

    this.formSelectGroup = this.fb.group({
      selectWorkout: this.fb.control('')
    });
  }

  addWorkout() {
    const selectWorkout = this.formSelectGroup.get('selectWorkout').value as Workout;
    this.listOfScheduledWorkouts.push(this.fb.group({
      name: '',
      workoutId: selectWorkout.id,
      listOfWorkoutPhases: this.fb.array(selectWorkout.listOfPhases.map(phase => this.convertWorkoutPhaseToInstancePhase(phase)))
    }));
  }

  private convertWorkoutPhaseToInstancePhase(workoutPhase: WorkoutPhase) {
    return new WorkoutInstancePhase(workoutPhase.name,
      workoutPhase.listOfExercises.map(exercise => new WorkoutSet(exercise)), workoutPhase.isOptional);
  }

  private initListOfSchedulerWorkout(listOfScheduledWorkouts: WorkoutInstance[]) {
    this.listOfScheduledWorkouts = this.fb.array([]);
    if (listOfScheduledWorkouts) {
      listOfScheduledWorkouts.forEach(workout => {
        this.listOfScheduledWorkouts.push(this.fb.group({
          name: workout.name,
          workoutId: this.fb.control(workout.workoutId),
          listOfWorkoutPhases: this.fb.array(workout.listOfWorkoutPhases)
        }));
      });
    }
    return this.listOfScheduledWorkouts;
  }


  removeWorkout(index: number) {
    this.listOfScheduledWorkouts.removeAt(index);
  }


}
