import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MetadataService} from '../../../../../share/services/metadata.service';
import {AbstractFormComponent} from '../../../form-components/model/abstract-form-component';
import {Observable} from 'rxjs';
import {CharacteristicPropertyFilter} from '../../../../../trainings/models/characteristic-property-filter';
import {WorkoutProgramCharacteristic} from '../../../../../trainings/models/workout-programs';

@Component({
  selector: 'app-entity-characteristic',
  templateUrl: './entity-characteristic.component.html',
  styleUrls: ['./entity-characteristic.component.scss']
})
export class EntityCharacteristicComponent extends AbstractFormComponent implements OnInit {

  @Input()
  characteristic: WorkoutProgramCharacteristic;

  @Input()
  filterEndpoint: string;

  @Input()
  entityName: string;

  characteristicForm: FormGroup;
  valueOFProperties$: Observable<CharacteristicPropertyFilter[]>;

  constructor(private fb: FormBuilder, private metadataService: MetadataService) {
    super();
  }

  ngOnInit(): void {
    this.valueOFProperties$ = this.metadataService.getValueOfCharacteristicByEntityName(this.filterEndpoint);
    this.characteristicForm = this.fb.group({});
    this.parent.setControl('characteristic', this.characteristicForm);
  }

}
