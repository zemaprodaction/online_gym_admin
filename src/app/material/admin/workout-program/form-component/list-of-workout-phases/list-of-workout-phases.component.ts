import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../../../form-components/model/abstract-form-component';
import {FormArray, FormBuilder} from '@angular/forms';
import {WorkoutInstancePhase} from '../../../../../trainings/models/workout/workout-instance-phase';

@Component({
  selector: 'app-list-of-workout-phases',
  templateUrl: './list-of-workout-phases.component.html',
  styleUrls: ['./list-of-workout-phases.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListOfWorkoutPhasesComponent extends AbstractFormComponent implements OnInit {

  @Input()
  listOfWorkoutPhases: WorkoutInstancePhase[];


  constructor(private fb: FormBuilder) {
    super();
  }

  listOfWorkoutPhasesFormArray: FormArray;

  ngOnInit() {
    this.parent.setControl('listOfWorkoutPhases', this.initWorkoutPhases());

  }

  private initWorkoutPhases() {
    this.listOfWorkoutPhasesFormArray = this.fb.array([]);
    if (this.listOfWorkoutPhases) {
      this.listOfWorkoutPhases.forEach(phase => {
        this.createWorkoutPhaseItem(phase);
      });
    }
    return this.listOfWorkoutPhasesFormArray;
  }

  private createWorkoutPhaseItem(phase: WorkoutInstancePhase) {
    this.listOfWorkoutPhasesFormArray.push(this.fb.group({
      name: [phase.name],
      listOfSets: this.fb.array(phase.listOfSets),
      isOptional: phase.isOptional
    }));
  }

  removePhase(phaseIndex) {
    this.listOfWorkoutPhasesFormArray.removeAt(phaseIndex);
  }
}
