import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../../../form-components/model/abstract-form-component';
import {FormArray, FormBuilder} from '@angular/forms';
import {WorkoutWeek} from '../../../../../trainings/models/workout-programs';
import * as fromStore from '../../../../../trainings/store';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-list-of-weeks',
  templateUrl: './list-of-weeks.component.html',
  styleUrls: ['./list-of-weeks.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListOfWeeksComponent extends AbstractFormComponent implements OnInit {

  @Input()
  weekList: WorkoutWeek[];

  weekListFormArray: FormArray;

  constructor(private fb: FormBuilder, private store: Store<fromStore.TrainingState>) {
    super();
  }

  get weeks(): any[] {
    return (this.parent.get('listOfWeeks') as FormArray).controls;
  }

  steps: Function;

  ngOnInit() {
    this.weekList = this.weekList ? this.weekList : [new WorkoutWeek()];
    this.store.dispatch(fromStore.loadWorkouts());
    this.steps = (weekList) => {
      return weekList.map((week, index) => `Неделя ${index + 1}`);
    };
    this.parent.setControl('listOfWeeks', this.iniListOfWeeks(this.weekList));
  }

  private iniListOfWeeks(weekList: WorkoutWeek[]) {
    this.weekListFormArray = this.fb.array([]);
    if (weekList) {
      weekList.forEach((w) => {
        this.weekListFormArray.push(this.fb.group({}));
      });
    }
    return this.weekListFormArray;
  }

  removeWeek(index: number) {
    this.weekListFormArray.removeAt(index);
    this.weekListFormArray.controls = [...this.weekListFormArray.controls];
  }

  addWeek() {
    this.weekListFormArray.push((this.fb.group({
      listOfDays: this.fb.array([])
    })));
    this.weekList = [...this.weekList, new WorkoutWeek()];
  }

  clone() {
    const lastWeek = this.weekListFormArray.value[this.weekListFormArray.value.length - 1];
    this.weekListFormArray.push((this.fb.group({
      listOfDays: this.fb.array([...lastWeek.listOfDays])
    })));
    this.weekList = [...this.weekList, new WorkoutWeek([...lastWeek.listOfDays])];
  }


}
