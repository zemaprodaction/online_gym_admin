import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {CrudAbstractAdminPanelComponent} from '../../crud-abstract-admin-panel/crud-abstract-admin-panel.component';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import * as fromStore from '../../../../trainings/store';
import {DialogConfigurationService} from '../../servises/servises-module/dialog-configuration.service';
import {CommonTrainingInfo} from '../../../../trainings/models/сommonTrainingInfo';
import {SaveUpdateWorkoutProgramFormComponent} from '../dialogs/save/save-form.component';

@Component({
  selector: 'app-workout-program-admin',
  templateUrl: './workout-program-admin.component.html',
  styleUrls: ['./workout-program-admin.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutProgramAdminComponent extends  CrudAbstractAdminPanelComponent implements OnInit {
  displayedColumns = ['id'];

  constructor(protected dialog: MatDialog, protected store: Store<fromStore.TrainingState>,
              private config: DialogConfigurationService) {
    super(dialog, store);
  }

  ngOnInit() {
  }

  get saveForm(): any {
    return SaveUpdateWorkoutProgramFormComponent;
  }

  protected dialogCreateConfig(): MatDialogConfig {
    const workoutProgram = {common: new CommonTrainingInfo()};
    return {data: {header: 'New Workout Program', workoutProgram}, ...this.config.getLargeDialogConfig()};
  }

  protected dialogUpdateConfig(row: any): Observable<{ data: {} }> {
    return this.getInfoFromStore(row, fromStore.getWorkoutProgramByID,
      {...this.config.getLargeDialogConfig(), header: row.name});
  }

}
