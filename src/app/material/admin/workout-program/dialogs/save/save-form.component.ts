import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FormFieldModel} from '../../../form-components/model/form-field-model';
import {WorkoutProgram} from '../../../../../trainings/models/workout-programs';
import {Store} from '@ngrx/store';
import * as fromStore from '../../../../../trainings/store';
import {loadMeasureTypes} from '../../../../../trainings/store/actions/measure-type.action';


@Component({
  selector: 'app-add',
  templateUrl: './save-form.component.html',
  styleUrls: ['./save-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaveUpdateWorkoutProgramFormComponent implements OnInit {

  form: FormGroup;
  id: FormFieldModel = {name: 'Id', formControlName: 'id', placeholder: 'Enter Id'};
  workoutProgram: WorkoutProgram;

  constructor(public dialogRef: MatDialogRef<SaveUpdateWorkoutProgramFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { row: WorkoutProgram, header: string },
              private fb: FormBuilder, private store: Store<fromStore.TrainingState>
  ) {
    this.workoutProgram = this.data.row ? this.data.row : {
      characteristic: {
        workoutPlaces: {
          value: [],
          nativeValue: [],
        }
      }
    };
    this.form = this.initForm(this.workoutProgram);
  }

  ngOnInit() {
    this.store.dispatch(loadMeasureTypes());
  }

  getFormValue() {
    return this.form.getRawValue();
  }

  private initForm(workoutProgram: WorkoutProgram) {
    return this.fb.group({
      id: [workoutProgram.id ? workoutProgram.id : ''],
      isPublished: [workoutProgram.isPublished ? workoutProgram.isPublished : '']
    });
  }

}
