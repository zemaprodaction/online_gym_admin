import {WorkoutProgramAdminComponent} from './workout-program-admin/workout-program-admin.component';

export const workoutProgramTableAdminComponents = [WorkoutProgramAdminComponent];

export * from './workout-program-admin/workout-program-admin.component';
