import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup} from '@angular/forms';
import {WorkoutProgram} from '../../../../../trainings/models/workout-programs';
import {UserKeycloak} from '../../../../../trainings/models/user-keycloak';
import {StatisticService} from '../../../../../share/services/statistic.service';
import {take} from 'rxjs/operators';


@Component({
  selector: 'app-add',
  templateUrl: './save-form.component.html',
  styleUrls: ['./save-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserDialogComponent implements OnInit {

  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<UserDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { row: UserKeycloak, header: string },
              private fb: FormBuilder, private statisticService: StatisticService
  ) {
  }

  ngOnInit() {
  }

  getFormValue() {
    return this.form.getRawValue();
  }

  private initForm(workoutProgram: WorkoutProgram) {
    return this.fb.group({
      id: [workoutProgram.id ? workoutProgram.id : ''],
    });
  }

  removeAllStatistic() {
    this.statisticService.removeAllUserStatistic(this.data.row.id).pipe(
      take(1),
    ).subscribe();
  }
}
