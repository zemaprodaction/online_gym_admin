import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {CrudAbstractAdminPanelComponent} from '../crud-abstract-admin-panel/crud-abstract-admin-panel.component';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {Store} from '@ngrx/store';
import * as fromStore from '../../../trainings/store';
import {DialogConfigurationService} from '../servises/servises-module/dialog-configuration.service';
import {WorkoutProgram} from '../../../trainings/models/workout-programs';
import {CommonTrainingInfo} from '../../../trainings/models/сommonTrainingInfo';
import {Observable, of} from 'rxjs';
import {UserDialogComponent} from './dialogs/save/save-form.component';

@Component({
  selector: 'app-user-admin',
  templateUrl: './user-admin.component.html',
  styleUrls: ['./user-admin.component.scss']
  ,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserAdminComponent extends CrudAbstractAdminPanelComponent implements OnInit {

  displayedColumns = ['name', 'id'];

  constructor(protected dialog: MatDialog, protected store: Store<fromStore.TrainingState>,
              private config: DialogConfigurationService) {
    super(dialog, store);
  }

  ngOnInit(): void {
  }

  get saveForm(): any {
    return UserDialogComponent;
  }

  protected dialogCreateConfig(): MatDialogConfig {
    const workoutProgram: WorkoutProgram = {common: new CommonTrainingInfo()};
    return {data: {header: 'New Workout Program', workoutProgram}, ...this.config.getLargeDialogConfig()};
  }

  protected dialogUpdateConfig(row: any): Observable<{ data: {} }> {
    return of({data: {row: row}});
  }

}
